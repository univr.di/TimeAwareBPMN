/**
 * Custom module used to catch events generated in the Panel or triggered by the modules in temporal-plugins-client
 * and perform actions like update the interface by triggering a different event
 */

import { is } from "bpmn-js/lib/util/ModelUtil";
import connect from "diagram-js/lib/features/connect";
import { get } from "jquery";

export default function CustomEvents(eventBus, commandStack, elementRegistry, modeling) {

  /**
   * Calls the function that updates the overlay-lavel of the sequenceFlow if it is connected to XOR
   * @param {*} connection 
   * @param {*} sourceRef 
   * @param {*} targetRef 
   */
  function checkChangeConnectionXOR(connection, sourceRef, targetRef) {
    var xorElement, i, tmpElemnet, targetElement;
    // Check if connection sourceRef and targetRef are connected to XOR to update overlay-label
    // is yes, check all the inputs and outputs 
    if (sourceRef && sourceRef.$type.includes('Gateway')) {
      xorElement = elementRegistry.get(sourceRef.id);
      if (xorElement) {
        for (i = 0; i < xorElement.businessObject.outgoing.length; i++) {
          tmpElemnet = xorElement.businessObject.outgoing[i];
          targetElement = elementRegistry.get(tmpElemnet.id);
          window.bpmnjs.updateLabelOverlay(targetElement);
        }
        for (i = 0; i < xorElement.businessObject.incoming.length; i++) {
          tmpElemnet = xorElement.businessObject.incoming[i];
          targetElement = elementRegistry.get(tmpElemnet.id);
          window.bpmnjs.updateLabelOverlay(targetElement);
        }
        eventBus.fire('element.changed', { element: xorElement });
      }
    }
    if (targetRef && targetRef.$type.includes('Gateway')) {
      xorElement = elementRegistry.get(targetRef.id);
      if (xorElement) {
        if(xorElement.businessObject.outgoing!=undefined){
          for (i = 0; i < xorElement.businessObject.outgoing.length; i++) {
            tmpElemnet = xorElement.businessObject.outgoing[i];
            targetElement = elementRegistry.get(tmpElemnet.id);
            window.bpmnjs.updateLabelOverlay(targetElement);
          }
        }
        if(xorElement.businessObject.incoming!=undefined){
          for (i = 0; i < xorElement.businessObject.incoming.length; i++) {
            tmpElemnet = xorElement.businessObject.incoming[i];
            targetElement = elementRegistry.get(tmpElemnet.id);
            window.bpmnjs.updateLabelOverlay(targetElement);
          }
        } 
        eventBus.fire('element.changed', { element: xorElement });

      }
    }
  }

  // Catch any change of a sequenceFlow 
  eventBus.on('connection.changed', (event) => {
    var currentElement = event.element;
    checkChangeConnectionXOR(currentElement, currentElement.businessObject.sourceRef, currentElement.businessObject.targetRef);
  });

  // Catch before a sequenceFlow is connected to other element
  eventBus.on('commandStack.connection.reconnect.preExecute', (event) => {

    const { context } = event;
    const { connection } = context;
    window.connectionToRemove = {
      connectionId: connection.businessObject.id,
      sourceRef: connection.businessObject.sourceRef,
      targetRef: connection.businessObject.targetRef
    };

  });


  // Catch before a sequenceFlow is deleted
  eventBus.on('commandStack.connection.delete.preExecute', (event) => {
    const { context } = event;
    const { connection } = context;
    window.connectionToRemove = {
      connectionId: connection.businessObject.id,
      sourceRef: connection.businessObject.sourceRef,
      targetRef: connection.businessObject.targetRef
    };
  });


  // Catch any change of a sequenceFlow 
  eventBus.on('element.changed', (event) => {
    var currentElement = event.element;

    if (is(currentElement, 'bpmn:SequenceFlow')) {
      // Check if the element that changed is a sequenceFlow that was reconnected or deleted 
      if (window.connectionToRemove && window.connectionToRemove.connectionId == currentElement.businessObject.id) {
        checkChangeConnectionXOR(currentElement, window.connectionToRemove.sourceRef, window.connectionToRemove.targetRef);
        window.connectionToRemove = undefined;
      }
    }
  });

  // catch change event on properties panel
  eventBus.on('propertiesPanel.changed', (event) => {

    var currentElement = event.current.element;

    // if (is(currentElement, "bpmn:ExclusiveGateway")) {
    //   if (currentElement.businessObject.outgoing != undefined) {
    //     for (let i = 0; i < currentElement.businessObject.outgoing.length; i++) {
    //       let outgoingElemnet = currentElement.businessObject.outgoing[i];
    //       let targetElement = elementRegistry.get(outgoingElemnet.id);
    //       // eventBus.fire('element.changed', { element: targetElement });


    //       // // To add/update the label of the relative constraint 
    //       // if (is(targetElement, "bpmn:SequenceFlow")) {
    //       //   console.log('targetElement in exclusiveGateway');

    //       //   window.bpmnjs.updateLabelOverlay(targetElement);
    //       // }

    //       // modeling.updateProperties(targetElement, {
    //       //   // maxDuration: newValue,
    //       //   // updated: true
    //       // });

    //     }
    //   }
    // }

    // To add/update the label of the relative constraint 
    if (is(currentElement, "custom:connection")) {
      window.bpmnjs.updateLabelOverlay(currentElement);
    }
    // To add/update the label of the relative constraint 
    if (is(currentElement, "bpmn:SequenceFlow")) {
      window.bpmnjs.updateLabelOverlay(currentElement);
    }


    // if (is(currentElement, 'bpmn:IntermediateCatchEvent')) {
    //   if (currentElement.businessObject.eventDefinitions && currentElement.businessObject.eventDefinitions.length > 0) {
    //     let strOptions = ['bpmn:TimerEventDefinition'];
    //     if (strOptions.includes(currentElement.businessObject.eventDefinitions[0].$type)) {
    //       //Update element in BPMN
    //       let tempElement = window.bpmnjs.get('elementRegistry').get(currentElement.businessObject.id);
    //       let minDuration = window.bpmnjs.getExtensionElementValue(tempElement, 'typeName', 'minDuration');
    //       let maxDuration = window.bpmnjs.getExtensionElementValue(tempElement, 'typeName', 'maxDuration');
    //       if (minDuration != maxDuration) { // To prevent generation of infinite events
    //         window.bpmnjs.setExtensionElementValue(tempElement, 'typeName', 'minDuration', maxDuration);
    //         try {
    //           eventBus.fire('element.changed', { element: tempElement });

    //         } catch (error) {
    //           console.log('Error when fire element.changed ' + tempElement.businessObject.id);
    //         }
    //       }
    //     }
    //   }
    // }



    if (is(currentElement, 'bpmn:BoundaryEvent')) {
      if (currentElement.businessObject.eventDefinitions && currentElement.businessObject.eventDefinitions.length > 0) {
        let strOptions = ['bpmn:MessageEventDefinition'];
        if (strOptions.includes(currentElement.businessObject.eventDefinitions[0].$type)) {
          //Update element in BPMN. minDuration has to be 0          
          let tempElement = window.bpmnjs.get('elementRegistry').get(currentElement.businessObject.id);
          let minDuration = window.bpmnjs.getExtensionElementValue(tempElement, 'typeName', 'minDuration');
          if (minDuration != 0) { // To prevent generation of infinite events
            window.bpmnjs.setExtensionElementValue(tempElement, 'typeName', 'minDuration', 0);
            try {
              eventBus.fire('element.changed', { element: tempElement });

            } catch (error) {
              console.log('Error when fire element.changed ' + tempElement.businessObject.id);
            }
          }
        }
      }
    }
  });

  // eventBus.on('tempcon.changed', (event) => {
  //   var currentElement = event.element;
  //   if (is(currentElement, "bpmn:ParallelGateway")) {
  //     if (currentElement.businessObject.incoming != undefined) {
  //       for (let i = 0; i < currentElement.businessObject.incoming.length; i++) {
  //         let incomingElement = currentElement.businessObject.incoming[i];
  //         let targetElement = elementRegistry.get(incomingElement.id);
  //         if (targetElement) {
  //           try {
  //             eventBus.fire('element.changed', { element: targetElement });

  //           } catch (error) {
  //             console.log('Error when fire element.changed ' + incomingElement.id);
  //           }
  //         }
  //       }
  //     }
  //   }
  // });
}

CustomEvents.$inject = ['eventBus', 'commandStack', 'elementRegistry', 'modeling'];
