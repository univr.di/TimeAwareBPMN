/*global test, expect*/

/** 
 * Tests of the functions in TA_BPMN_editor/app/temporal-modeler/temporal-plugins-client/CSTNU/bpmn2cstnu.js
 *  
 * Unit test are performed on most of the functions but in: 
 *   createBoundaryNode_createEdgeAsBs,
 *   createBoundaryNode_getPropositionalLabels,
 *   createBoundaryNode_createNodeANDJoin,
 *   createBoundaryNode,
 *   setElements
 * which are tested in integration tests performed with the function bpmn2cstnu
 * 
 * To performs the test auxiliar functions are used:
 *  
 *    getExample01(idElement): Contains a BPMN XML string from which the idElement can be requested 
 *    getDiagramToTest(idDiagram): Contains different examples of BPMN XML string  
 * 
 * Mario Ocampo
 * 20211231
 */


const { checkMinMax_elements,
  bpmn2cstnu,
  initAuxObjects,
  checkMinMax_sequenceFlow,
  checkMinMax_relativeConstraint,
  getXY,
  checkSplitJoinFromXML,
  checkIfIsGateway_isOK,
  checkObservedPropositionInBoundaryEvents,
  setTwoNodesToEdges,
  createOneNode,
  getGraphNodeFromId,
  arraymove,
  setTwoEdges_sequenceFlow,
  setTwoEdges_relativeConstraint,
  getStart_xml,
  get_bpmnPlane,
  getExtensionElementValueFromXML,
  getExtensionRelativeConstraintFromXML,
  getRelativeConstraintValueFromXML
  // createBoundaryNode_createEdgeAsBs,
  // createBoundaryNode_getPropositionalLabels,
  // createBoundaryNode_createNodeANDJoin,
  // createBoundaryNode,
  // setElements,
} = require("../bpmn2cstnu.js");

// ------ checkMinMax_elements ------ //
test('Test checkMinMax_elements valid configuration (contingent)', () => {
  let element = getExample01("T1");

  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_elements(element, myLogObj, "contingent");
  expect(tmpObj).toStrictEqual({ minDuration: '2', maxDuration: '3', okVals: true });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:userTask Identify Payment Method (T1)  (T1)  
      bpmn2:userTask Identify Payment Method (T1)  (T1)  minDuration 2 
      bpmn2:userTask Identify Payment Method (T1)  (T1)  maxDuration 3`, errors: "", warnings: ""
  }));
});

test('Test checkMinMax_elements invalid configuration (contingent)', () => {
  let element = getExample01("T3");
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_elements(element, myLogObj, "contingent");
  expect(tmpObj).toStrictEqual({ minDuration: '14', maxDuration: '6', okVals: false });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:userTask Accept Cash (T3)  (T3)  
      bpmn2:userTask Accept Cash (T3)  (T3)  minDuration 14`,
    errors: `bpmn2:userTask Accept Cash (T3)  (T3) 
     	maxDuration (6) should be > minDuration (14)`, warnings: ""
  }));
});

test('Test checkMinMax_elements valid configuration (non-contingent)', () => {
  let element = getExample01("G2");
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_elements(element, myLogObj, "normal");
  expect(tmpObj).toStrictEqual({ minDuration: '1', maxDuration: '2', okVals: true });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:exclusiveGateway G2  (G2)  
      bpmn2:exclusiveGateway G2  (G2)  minDuration 1 
      bpmn2:exclusiveGateway G2  (G2)  maxDuration 2`, errors: "", warnings: ""
  }));
});

test('Test checkMinMax_elements invalid configuration (non-contingent)', () => {
  let element = getExample01("G1");
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_elements(element, myLogObj, "normal");
  expect(tmpObj).toStrictEqual({ minDuration: '12', maxDuration: '4', okVals: false });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:exclusiveGateway Credit Card Payment? (G1)  (G1)  
      bpmn2:exclusiveGateway Credit Card Payment? (G1)  (G1)  minDuration 12`,
    errors: `bpmn2:exclusiveGateway Credit Card Payment? (G1)  (G1) 
     	maxDuration (4) should be >= minDuration (12)`, warnings: ""
  }));
});

test('Test checkMinMax_elements of null', () => {
  let element = getExample01("nullObject");
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_elements(element, myLogObj, "contingent");
  expect(tmpObj).toStrictEqual({ minDuration: undefined, maxDuration: undefined, okVals: false });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({ log: "", errors: "", warnings: "" }));
});

test('Test checkMinMax_elements of element with NaNs', () => {
  let element = getExample01("G4");
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_elements(element, myLogObj, "normal");
  expect(tmpObj).toStrictEqual({ minDuration: "a", maxDuration: "b", okVals: false });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:parallelGateway G4  (G4)`,
    errors: `bpmn2:parallelGateway G4  (G4) 
      minDuration (a) is NaN 
      maxDuration (b) is NaN`,
    warnings: ""
  }));
});

test('Test checkMinMax_elements of element with float values', () => {
  let element = getExample01("T2");
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_elements(element, myLogObj, "contingent");
  expect(tmpObj).toStrictEqual({ minDuration: "1.1", maxDuration: "2.2", okVals: false });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:userTask Authorize Credit Card (T2)  (T2)`,
    errors: `bpmn2:userTask Authorize Credit Card (T2)  (T2) 
     	minDuration (1.1) is not integer 
     	maxDuration (2.2) is not integer `,
    warnings: ""
  }));
});

// ----  checkMinMax_sequenceFlow ---- 
test('Test checkMinMax_sequenceFlow of default configuration', () => {
  let element = getExample01("Flow_0wuk23o");
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_sequenceFlow(element, myLogObj, "normal");
  expect(tmpObj).toStrictEqual({ minDuration: 0, maxDuration: Infinity, okVals: true });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:sequenceFlow [Flow_0wuk23o]  
      bpmn2:sequenceFlow  [Flow_0wuk23o]  minDuration 0 
      bpmn2:sequenceFlow  [Flow_0wuk23o]  maxDuration Infinity`, 
    errors: "", warnings: ""
  }));
});

test('Test checkMinMax_sequenceFlow of valid configuration', () => {
  let element = getExample01("Flow_0aync76");
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_sequenceFlow(element, myLogObj, "normal");
  expect(tmpObj).toStrictEqual({ minDuration: "1", maxDuration: "2", okVals: true });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:sequenceFlow [Flow_0aync76]  
      bpmn2:sequenceFlow [Flow_0aync76]  minDuration 1 
      bpmn2:sequenceFlow [Flow_0aync76]  maxDuration 2`, 
    errors: "", warnings: ""
  }));
});

test('Test checkMinMax_sequenceFlow of null', () => {
  let element = getExample01("nullObject");
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_sequenceFlow(element, myLogObj, "normal");
  expect(tmpObj).toStrictEqual({ minDuration: undefined, maxDuration: undefined, okVals: false });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({ log: "", errors: "", warnings: "" }));
});

test('Test checkMinMax_sequenceFlow of element with NaNs', () => {
  let element = getExample01("G4");
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_sequenceFlow(element, myLogObj, "normal");
  expect(tmpObj).toStrictEqual({ minDuration: "a", maxDuration: "b", okVals: false });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:parallelGateway G4 [G4]`,
    errors: `bpmn2:parallelGateway G4 [G4] 
   	minDuration (a) is NaN maxDuration (b) is NaN `, warnings: ""
  }));
});

test('Test checkMinMax_sequenceFlow of element with float values', () => {
  let element = getExample01("Flow_19suy4z");
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_sequenceFlow(element, myLogObj, "normal");
  expect(tmpObj).toStrictEqual({ minDuration: "1.1", maxDuration: "4.4", okVals: false });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:sequenceFlow True [Flow_19suy4z]`,
    errors: `bpmn2:sequenceFlow True [Flow_19suy4z] 
   	minDuration (1.1) is not integer maxDuration (4.4) is not integer`, warnings: ""
  }));
});

// ----  checkMinMax_relativeConstraint ---- 
test('Test checkMinMax_relativeConstraint', () => {
  let element = getExample01("T1");
  let relativeConstraint = getExtensionRelativeConstraintFromXML(element);
  let myLogObj = { log: "", errors: "", warnings: "" };
  let tmpObj = checkMinMax_relativeConstraint(relativeConstraint, myLogObj, "relative");
  expect(tmpObj).toStrictEqual({ minDuration: "2", maxDuration: "31", okVals: true });
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: ` RelativeConstraint_2v93 
      RelativeConstraint_2v93 minDuration 2 
      RelativeConstraint_2v93 maxDuration 31`, errors: "", warnings: ""
  }));
});


// ----  getRelativeConstraintValueFromXML ---- 
test('Test getRelativeConstraintValueFromXML', () => {
  let element = getExample01("T1");
  let relativeConstraint = getExtensionRelativeConstraintFromXML(element);
  let value = getRelativeConstraintValueFromXML(relativeConstraint, "minDuration");
  expect(value).toStrictEqual("2");
  value = getRelativeConstraintValueFromXML(relativeConstraint, "maxDuration");
  expect(value).toStrictEqual("31");
});


// ----  getExtensionElementValueFromXML ---- 
test('Test getExtensionElementValueFromXML', () => {
  let element = getExample01("T1");
  let value = getExtensionElementValueFromXML(element, "minDuration");
  expect(value).toStrictEqual("2");
  value = getExtensionElementValueFromXML(element, "maxDuration");
  expect(value).toStrictEqual("3");
});


// ----  getXY ---- 
test('Test getXY', () => {
  let xmlDoc = getExample01();
  let bpmnPlane = get_bpmnPlane(xmlDoc);
  let tmpObj = getXY(bpmnPlane, "T1");
  expect(tmpObj).toStrictEqual({ x: "170", y: "140" });
  tmpObj = getXY(bpmnPlane, "T3");
  expect(tmpObj).toStrictEqual({ x: "270", y: "250" });
  tmpObj = getXY(bpmnPlane, "G2");
  expect(tmpObj).toStrictEqual({ x: "445", y: "155" });
});

// ----  checkSplitJoinFromXML ---- 
test('Test checkSplitJoinFromXML  XOR split', () => {
  let element = getExample01("G1");
  let tmpObj = checkSplitJoinFromXML(element);
  expect(tmpObj).toStrictEqual("split");
});
test('Test checkSplitJoinFromXML XOR join', () => {
  let element = getExample01("G2");
  let tmpObj = checkSplitJoinFromXML(element);
  expect(tmpObj).toStrictEqual("join");
});
test('Test checkSplitJoinFromXML Task', () => {
  let element = getExample01("T1");
  let tmpObj = checkSplitJoinFromXML(element);
  expect(tmpObj).toStrictEqual(undefined);
});

// ----  checkIfIsGateway_isOK ---- 
test('Test checkIfIsGateway_isOK XOR split', () => {
  let element, tmpObj;
  let [myObjs, myLogObj, countObjs] = initAuxObjects();
  element = getExample01("G1");
  myObjs[element.attributes.id.value] = { taskNumber: 1, nodeName: '', name: '', id: '', elementType: 'XOR', elementTypeNumber: 1, cstnuNodeIds: [], cstnuEdgeIds: [], inputs: [], outputs: [], edgeType: "normal", id_s: '', id_e: '' };
  tmpObj = checkIfIsGateway_isOK(element, myObjs, myLogObj, countObjs);
  expect(tmpObj).toStrictEqual(true);
});

test('Test checkIfIsGateway_isOK XOR join', () => {
  let element, myLogObj, countObjs, tmpObj;
  let myObjs = {};
  element = getExample01("G2");
  myObjs[element.attributes.id.value] = { taskNumber: 2, nodeName: '', name: '', id: '', elementType: 'XOR', elementTypeNumber: 2, cstnuNodeIds: [], cstnuEdgeIds: [], inputs: [], outputs: [], edgeType: "normal", id_s: '', id_e: '' };
  tmpObj = checkIfIsGateway_isOK(element, myObjs, myLogObj, countObjs);
  expect(tmpObj).toStrictEqual(true);
});

test('Test checkIfIsGateway_isOK Task', () => {
  let element, myLogObj, countObjs, tmpObj;
  let myObjs = {};
  element = getExample01("T1");
  myObjs[element.attributes.id.value] = { taskNumber: 3, nodeName: '', name: '', id: '', elementType: 'TASK', elementTypeNumber: 1, cstnuNodeIds: [], cstnuEdgeIds: [], inputs: [], outputs: [], edgeType: "contingent", id_s: '', id_e: '' };
  tmpObj = checkIfIsGateway_isOK(element, myObjs, myLogObj, countObjs);
  expect(tmpObj).toStrictEqual(true);
});

// ----  createOneNode ---- 
test('Test createOneNode', () => {
  let root = getStart_xml();
  let graph = root.ele("graph", { edgedefault: "directed" });

  let xmlDoc = getExample01();
  let bpmnPlane = get_bpmnPlane(xmlDoc);

  let element;
  let [myObjs, myLogObj, countObjs] = initAuxObjects();

  element = getExample01("StartEvent_1");
  let paramsNormal = { elementType: 'TASK', element, graph, bpmnPlane, "edgeType": "normal", myLogObj, countObjs, myObjs };

  createOneNode(paramsNormal);
  expect(myObjs).toStrictEqual({ "StartEvent_1": { "cstnuNodeIds": ["Z"], "edgeType": "normal", "elementType": "START", "elementTypeNumber": "", "id": "StartEvent_1", "idCSTNU": "START_", "id_node": "Z", "inputs": [], "name": "s ", "nodeName": "bpmn2:startEvent", "outputs": [], "taskNumber": 1 }, "arrows": {}, "edges_ids": {}, "nodeObservation": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E"], "relative": { "datainput": [], "dataoutput": [], "idElementsWithRelativeConstraints": [] } });
});

// ----  setTwoNodesToEdges ---- 
test('Test setTwoNodesToEdges', () => {
  let root = getStart_xml();
  let graph = root.ele("graph", { edgedefault: "directed" });

  let xmlDoc = getExample01();
  let bpmnPlane = get_bpmnPlane(xmlDoc);

  let element;
  let [myObjs, myLogObj, countObjs] = initAuxObjects();

  element = getExample01("G2");
  let paramsNormal = { elementType: 'TASK', element, graph, bpmnPlane, "edgeType": "normal", myLogObj, countObjs, myObjs };

  setTwoNodesToEdges(paramsNormal);
  expect(myObjs).toStrictEqual({ "G2": { "cstnuEdgeIds": ["S_TASK_1_G2-E_TASK_1_G2", "E_TASK_1_G2-S_TASK_1_G2"], "cstnuNodeIds": ["S_TASK_1_G2", "E_TASK_1_G2"], "edgeType": "normal", "elementType": "TASK", "elementTypeNumber": 1, "id": "G2", "id_e": "E_TASK_1_G2", "id_s": "S_TASK_1_G2", "inputs": [], "name": "G2 ", "nodeName": "bpmn2:exclusiveGateway", "obs": "join", "outputs": [], "taskNumber": 1 }, "arrows": {}, "edges_ids": { "E_TASK_1_G2-S_TASK_1_G2": { "elementIds": ["G2"], "occurrences": 1 }, "S_TASK_1_G2-E_TASK_1_G2": { "elementIds": ["G2"], "occurrences": 1 } }, "nodeObservation": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E"], "relative": { "datainput": [], "dataoutput": [], "idElementsWithRelativeConstraints": [] } });
  // graph is an JS object with circular structure
});

test('Test setTwoNodesToEdges', () => {
  let root = getStart_xml();
  let graph = root.ele("graph", { edgedefault: "directed" });

  let xmlDoc = getExample01();
  let bpmnPlane = get_bpmnPlane(xmlDoc);

  let element;
  let [myObjs, myLogObj, countObjs] = initAuxObjects();

  element = getExample01("T2");
  let paramsContingent = { elementType: 'TASK', element, graph, bpmnPlane, "edgeType": "contingent", myLogObj, countObjs, myObjs };

  setTwoNodesToEdges(paramsContingent);
  expect(myObjs).toStrictEqual({ "T2": { "cstnuEdgeIds": ["S_TASK_1_T2-E_TASK_1_T2", "E_TASK_1_T2-S_TASK_1_T2"], "cstnuNodeIds": ["S_TASK_1_T2", "E_TASK_1_T2"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 1, "id": "T2", "id_e": "E_TASK_1_T2", "id_s": "S_TASK_1_T2", "inputs": [], "name": "Authorize Credit Card (T2) ", "nodeName": "bpmn2:userTask", "outputs": [], "taskNumber": 1 }, "arrows": {}, "edges_ids": { "E_TASK_1_T2-S_TASK_1_T2": { "elementIds": ["T2"], "occurrences": 1 }, "S_TASK_1_T2-E_TASK_1_T2": { "elementIds": ["T2"], "occurrences": 1 } }, "nodeObservation": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E"], "relative": { "datainput": [], "dataoutput": [], "idElementsWithRelativeConstraints": [] } });
});

// ----  setTwoEdges_sequenceFlow ---- 
test('Test setTwoEdges_sequenceFlow', () => {
  let root = getStart_xml();
  let graph = root.ele("graph", { edgedefault: "directed" });

  let xmlDoc = getExample01();
  let bpmnPlane = get_bpmnPlane(xmlDoc);

  let element;
  let [myObjs, myLogObj, countObjs] = initAuxObjects();

  element = getExample01("G2");
  let paramsNormal = { elementType: 'TASK', element, graph, bpmnPlane, "edgeType": "normal", myLogObj, countObjs, myObjs };
  setTwoNodesToEdges(paramsNormal);

  element = getExample01("T1");
  let paramsContingent = { elementType: 'TASK', element, graph, bpmnPlane, "edgeType": "contingent", myLogObj, countObjs, myObjs };
  setTwoNodesToEdges(paramsContingent);

  element = getExample01("Flow_1h69eb3");
  paramsNormal = { element, graph, bpmnPlane, "edgeType": "normal", myLogObj, countObjs, myObjs };
  setTwoEdges_sequenceFlow(paramsNormal);

  expect(myObjs).toStrictEqual({ "G2": { "cstnuEdgeIds": ["S_TASK_1_G2-E_TASK_1_G2", "E_TASK_1_G2-S_TASK_1_G2"], "cstnuNodeIds": ["S_TASK_1_G2", "E_TASK_1_G2"], "edgeType": "normal", "elementType": "TASK", "elementTypeNumber": 1, "id": "G2", "id_e": "E_TASK_1_G2", "id_s": "S_TASK_1_G2", "inputs": [], "name": "G2 ", "nodeName": "bpmn2:exclusiveGateway", "obs": "join", "outputs": [], "taskNumber": 1 }, "T1": { "cstnuEdgeIds": ["S_TASK_2_T1-E_TASK_2_T1", "E_TASK_2_T1-S_TASK_2_T1"], "cstnuNodeIds": ["S_TASK_2_T1", "E_TASK_2_T1"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 2, "id": "T1", "id_e": "E_TASK_2_T1", "id_s": "S_TASK_2_T1", "inputs": [], "name": "Identify Payment Method (T1) ", "nodeName": "bpmn2:userTask", "outputs": [], "taskNumber": 2 }, "arrows": {}, "edges_ids": { "E_TASK_1_G2-S_TASK_1_G2": { "elementIds": ["G2"], "occurrences": 1 }, "E_TASK_2_T1-S_TASK_2_T1": { "elementIds": ["T1"], "occurrences": 1 }, "S_TASK_1_G2-E_TASK_1_G2": { "elementIds": ["G2"], "occurrences": 1 }, "S_TASK_2_T1-E_TASK_2_T1": { "elementIds": ["T1"], "occurrences": 1 } }, "nodeObservation": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E"], "relative": { "datainput": [], "dataoutput": [], "idElementsWithRelativeConstraints": ["T1"] } });
});

// ----  setTwoEdges_relativeConstraint ---- 
test('Test setTwoEdges_relativeConstraint', () => {
  // The relativeConstraint connects the elements T1 --> T4 
  let root = getStart_xml();
  let graph = root.ele("graph", { edgedefault: "directed" });

  let xmlDoc = getExample01();
  let bpmnPlane = get_bpmnPlane(xmlDoc);

  let element;
  let [myObjs, myLogObj, countObjs] = initAuxObjects();

  element = getExample01("T4");
  let paramsContingent = { elementType: 'TASK', element, graph, bpmnPlane, "edgeType": "contingent", myLogObj, countObjs, myObjs };
  setTwoNodesToEdges(paramsContingent);

  element = getExample01("T1");
  paramsContingent = { elementType: 'TASK', element, graph, bpmnPlane, "edgeType": "contingent", myLogObj, countObjs, myObjs };
  setTwoNodesToEdges(paramsContingent);

  // element = getExample01("T1"); 
  let paramsRelativeConstraint = { element, graph, bpmnPlane, "edgeType": "relative", myLogObj, countObjs, myObjs };
  setTwoEdges_relativeConstraint(paramsRelativeConstraint);

  expect(myObjs).toStrictEqual({ "T1": { "cstnuEdgeIds": ["S_TASK_2_T1-E_TASK_2_T1", "E_TASK_2_T1-S_TASK_2_T1"], "cstnuNodeIds": ["S_TASK_2_T1", "E_TASK_2_T1"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 2, "id": "T1", "id_e": "E_TASK_2_T1", "id_s": "S_TASK_2_T1", "inputs": [], "name": "Identify Payment Method (T1) ", "nodeName": "bpmn2:userTask", "outputs": [], "taskNumber": 2 }, "T4": { "cstnuEdgeIds": ["S_TASK_1_T4-E_TASK_1_T4", "E_TASK_1_T4-S_TASK_1_T4"], "cstnuNodeIds": ["S_TASK_1_T4", "E_TASK_1_T4"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 1, "id": "T4", "id_e": "E_TASK_1_T4", "id_s": "S_TASK_1_T4", "inputs": [], "name": "Deliver Package (T4) ", "nodeName": "bpmn2:userTask", "outputs": [], "taskNumber": 1 }, "arrows": {}, "edges_ids": { "E_TASK_1_T4-S_TASK_1_T4": { "elementIds": ["T4"], "occurrences": 1 }, "E_TASK_1_T4-S_TASK_2_T1-RelativeConstraint_2v93": { "elementIds": ["RelativeConstraint_2v93"], "occurrences": 1 }, "E_TASK_2_T1-S_TASK_2_T1": { "elementIds": ["T1"], "occurrences": 1 }, "S_TASK_1_T4-E_TASK_1_T4": { "elementIds": ["T4"], "occurrences": 1 }, "S_TASK_2_T1-E_TASK_1_T4-RelativeConstraint_2v93": { "elementIds": ["RelativeConstraint_2v93"], "occurrences": 1 }, "S_TASK_2_T1-E_TASK_2_T1": { "elementIds": ["T1"], "occurrences": 1 } }, "nodeObservation": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E"], "relative": { "datainput": [], "dataoutput": [], "idElementsWithRelativeConstraints": ["T1"] } });
});

// ----  getGraphNodeFromId ---- 
test('Test getGraphNodeFromId', () => {
  let root = getStart_xml();
  let graph = root.ele("graph", { edgedefault: "directed" });

  let xmlDoc = getExample01();
  let bpmnPlane = get_bpmnPlane(xmlDoc);

  let element;
  let [myObjs, myLogObj, countObjs] = initAuxObjects();

  element = getExample01("G2");

  let paramsNormal = { elementType: 'TASK', element, graph, bpmnPlane, "edgeType": "normal", myLogObj, countObjs, myObjs };

  setTwoNodesToEdges(paramsNormal);

  let [nodeNew_idx, nodeNew] = getGraphNodeFromId(graph, "S_TASK_1_G2");
  expect(nodeNew_idx).toStrictEqual(0);
  [nodeNew_idx, nodeNew] = getGraphNodeFromId(graph, "E_TASK_1_G2");
  expect(nodeNew_idx).toStrictEqual(1);

  element = getExample01("T1");
  let paramsContingent = { elementType: 'TASK', element, graph, bpmnPlane, "edgeType": "contingent", myLogObj, countObjs, myObjs };
  setTwoNodesToEdges(paramsContingent);

  [nodeNew_idx, nodeNew] = getGraphNodeFromId(graph, "S_TASK_2_T1");
  expect(nodeNew_idx).toStrictEqual(4);
  [nodeNew_idx, nodeNew] = getGraphNodeFromId(graph, "E_TASK_2_T1");
  expect(nodeNew_idx).toStrictEqual(5);
});

// ----  arraymove ---- 
test('Test arraymove', () => {
  let arr = ['a', 'b', 'c', 'd', 'e', 'f'];
  arraymove(arr, 1, 3);
  expect(arr).toStrictEqual(["a", "c", "d", "b", "e", "f"]);
  arraymove(arr, 3, 1);
  expect(arr).toStrictEqual(['a', 'b', 'c', 'd', 'e', 'f']);
  arraymove(arr, 0, 3);
  expect(arr).toStrictEqual(['b', 'c', 'd', 'a', 'e', 'f']);
  arraymove(arr, 3, 0);
  expect(arr).toStrictEqual(['a', 'b', 'c', 'd', 'e', 'f']);
});

// ----  checkObservedPropositionInBoundaryEvents ---- 
test('Test checkObservedPropositionInBoundaryEvents', () => {
  // If the element is BoundaryEvent, check if this has an observerdProposition 
  let [myObjs, myLogObj, countObjs] = initAuxObjects();
  let { diagram, xmlDoc, diagram_translated } = getDiagramToTest("xor_bei_xor_tc");
  let element = xmlDoc.getElementById("BoundaryEvent_01");
  myObjs["BoundaryEvent_01"] = {};
  let value = checkObservedPropositionInBoundaryEvents(element, myObjs, myLogObj, countObjs);
  expect(value).toStrictEqual(true);

  element = xmlDoc.getElementById("G3");
  myObjs["G3"] = {};
  value = checkObservedPropositionInBoundaryEvents(element, myObjs, myLogObj, countObjs);
  expect(value).toStrictEqual(true);
});


// ---- bpmn2cstnu ----
test('Test bpmn2cstnu of Example01 (corrected)', () => {
  let { diagram, xmlDoc, diagram_translated } = getDiagramToTest("example01");
  let { xmlString, myLogObj, countObjs, myObjs, textMessage } = bpmn2cstnu(diagram, xmlDoc);
  expect(xmlString).toStrictEqual(diagram_translated);
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:exclusiveGateway G2  (G2)  
    bpmn2:exclusiveGateway G2  (G2)  minDuration 1 
    bpmn2:exclusiveGateway G2  (G2)  maxDuration 2 
    bpmn2:userTask Accept Cash (T3)  (T3)  
    bpmn2:userTask Accept Cash (T3)  (T3)  minDuration 1 
    bpmn2:userTask Accept Cash (T3)  (T3)  maxDuration 6 
    bpmn2:userTask Deliver Package (T4)  (T4)  
    bpmn2:userTask Deliver Package (T4)  (T4)  minDuration 1 
    bpmn2:userTask Deliver Package (T4)  (T4)  maxDuration 4 
    bpmn2:intermediateCatchEvent Payment authorized (IE1)  (IE1)  
    bpmn2:intermediateCatchEvent Payment authorized (IE1)  (IE1)  minDuration 1 
    bpmn2:intermediateCatchEvent Payment authorized (IE1)  (IE1)  maxDuration 3 
    bpmn2:userTask Identify Payment Method (T1)  (T1)  
    bpmn2:userTask Identify Payment Method (T1)  (T1)  minDuration 2 
    bpmn2:userTask Identify Payment Method (T1)  (T1)  maxDuration 3 
    bpmn2:userTask Authorize Credit Card (T2)  (T2)  
    bpmn2:userTask Authorize Credit Card (T2)  (T2)  minDuration 1 
    bpmn2:userTask Authorize Credit Card (T2)  (T2)  maxDuration 2 
    bpmn2:exclusiveGateway Credit Card Payment? (G1)  (G1)  
    bpmn2:exclusiveGateway Credit Card Payment? (G1)  (G1)  minDuration 2 
    bpmn2:exclusiveGateway Credit Card Payment? (G1)  (G1)  maxDuration 4 
    bpmn2:sequenceFlow 
  [Flow_1h69eb3]  
    bpmn2:sequenceFlow 
   [Flow_1h69eb3]  minDuration 0 
    bpmn2:sequenceFlow 
   [Flow_1h69eb3]  maxDuration Infinity 
    bpmn2:sequenceFlow 
   [Flow_0wuk23o]  
    bpmn2:sequenceFlow 
   [Flow_0wuk23o]  minDuration 0 
    bpmn2:sequenceFlow 
   [Flow_0wuk23o]  maxDuration Infinity 
    bpmn2:sequenceFlow 
   [Flow_0pp9mni]  
    bpmn2:sequenceFlow 
   [Flow_0pp9mni]  minDuration 0 
    bpmn2:sequenceFlow 
   [Flow_0pp9mni]  maxDuration Infinity 
    bpmn2:sequenceFlow 
   [Flow_1qye66p]  
    bpmn2:sequenceFlow 
   [Flow_1qye66p]  minDuration 0 
    bpmn2:sequenceFlow 
   [Flow_1qye66p]  maxDuration Infinity 
    bpmn2:sequenceFlow 
   [Flow_1y1s5dn]  
    bpmn2:sequenceFlow 
   [Flow_1y1s5dn]  minDuration 0 
    bpmn2:sequenceFlow 
   [Flow_1y1s5dn]  maxDuration Infinity 
    bpmn2:sequenceFlow 
   [Flow_0aync76]  
    bpmn2:sequenceFlow 
   [Flow_0aync76]  minDuration 1 
    bpmn2:sequenceFlow 
   [Flow_0aync76]  maxDuration 2 
    bpmn2:sequenceFlow True 
   [Flow_19suy4z]  
    bpmn2:sequenceFlow True 
   [Flow_19suy4z]  minDuration 1 
    bpmn2:sequenceFlow True 
   [Flow_19suy4z]  maxDuration 4 
    bpmn2:sequenceFlow False 
   [Flow_1t58voa]  
    bpmn2:sequenceFlow False 
   [Flow_1t58voa]  minDuration 1 
    bpmn2:sequenceFlow False 
   [Flow_1t58voa]  maxDuration 3 
    bpmn2:sequenceFlow 
   [Flow_1okivbo]  
    bpmn2:sequenceFlow 
   [Flow_1okivbo]  minDuration 0 
    bpmn2:sequenceFlow 
   [Flow_1okivbo]  maxDuration Infinity 
    RelativeConstraint_2v93 
    RelativeConstraint_2v93 minDuration 2 
    RelativeConstraint_2v93 maxDuration 15`,
    errors: `Elements with error: 0`, warnings: `Elements with warning: 0`
  }));
  expect(countObjs).toStrictEqual({
    "TASK": 5, "XOR": 2, "boundaryEvents": 0,
    "boundaryEventsTotal": 0, "edges": 28, "elementsWithError": 0, "elementsWithWarning": 0,
    "endEvents": 0, "endEventsTotal": 1, "nContingents": 5, "nObservedProposition": 1, "nodes": 16,
    "startEvents": 0, "startEventsTotal": 1, "tasks": 9
  });
  expect(myObjs).toStrictEqual({ "G1": { "cstnuEdgeIds": ["S_XOR_2_G1-E_XOR_2_G1", "E_XOR_2_G1-S_XOR_2_G1"], "cstnuNodeIds": ["S_XOR_2_G1", "E_XOR_2_G1"], "edgeType": "normal", "elementType": "XOR", "elementTypeNumber": 2, "id": "G1", "id_e": "E_XOR_2_G1", "id_s": "S_XOR_2_G1", "inputs": ["Flow_0aync76"], "name": "Credit Card Payment? (G1) ", "nodeName": "bpmn2:exclusiveGateway", "obs": "split", "observedProposition": "A", "outputs": ["Flow_19suy4z", "Flow_1t58voa"], "taskNumber": 7 }, "G2": { "cstnuEdgeIds": ["S_XOR_1_G2-E_XOR_1_G2", "E_XOR_1_G2-S_XOR_1_G2"], "cstnuNodeIds": ["S_XOR_1_G2", "E_XOR_1_G2"], "edgeType": "normal", "elementType": "XOR", "elementTypeNumber": 1, "id": "G2", "id_e": "E_XOR_1_G2", "id_s": "S_XOR_1_G2", "inputs": ["Flow_1h69eb3", "Flow_0wuk23o"], "name": "G2 ", "nodeName": "bpmn2:exclusiveGateway", "obs": "join", "outputs": ["Flow_1okivbo"], "taskNumber": 1 }, "IE1": { "cstnuEdgeIds": ["S_TASK_3_IE1-E_TASK_3_IE1", "E_TASK_3_IE1-S_TASK_3_IE1"], "cstnuNodeIds": ["S_TASK_3_IE1", "E_TASK_3_IE1"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 3, "id": "IE1", "id_e": "E_TASK_3_IE1", "id_s": "S_TASK_3_IE1", "inputs": ["Flow_1qye66p"], "name": "Payment authorized (IE1) ", "nodeName": "bpmn2:intermediateCatchEvent", "outputs": ["Flow_0wuk23o"], "taskNumber": 4 }, "StartEvent_1": { "cstnuNodeIds": ["Z"], "edgeType": "normal", "elementType": "START", "elementTypeNumber": "", "id": "StartEvent_1", "idCSTNU": "START_", "id_node": "Z", "inputs": [], "name": "s ", "nodeName": "bpmn2:startEvent", "outputs": ["Flow_1y1s5dn"], "taskNumber": 8 }, "T1": { "cstnuEdgeIds": ["S_TASK_4_T1-E_TASK_4_T1", "E_TASK_4_T1-S_TASK_4_T1"], "cstnuNodeIds": ["S_TASK_4_T1", "E_TASK_4_T1"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 4, "id": "T1", "id_e": "E_TASK_4_T1", "id_s": "S_TASK_4_T1", "inputs": ["Flow_1y1s5dn"], "name": "Identify Payment Method (T1) ", "nodeName": "bpmn2:userTask", "outputs": ["Flow_0aync76"], "taskNumber": 5 }, "T2": { "cstnuEdgeIds": ["S_TASK_5_T2-E_TASK_5_T2", "E_TASK_5_T2-S_TASK_5_T2"], "cstnuNodeIds": ["S_TASK_5_T2", "E_TASK_5_T2"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 5, "id": "T2", "id_e": "E_TASK_5_T2", "id_s": "S_TASK_5_T2", "inputs": ["Flow_19suy4z"], "name": "Authorize Credit Card (T2) ", "nodeName": "bpmn2:userTask", "outputs": ["Flow_1qye66p"], "taskNumber": 6 }, "T3": { "cstnuEdgeIds": ["S_TASK_1_T3-E_TASK_1_T3", "E_TASK_1_T3-S_TASK_1_T3"], "cstnuNodeIds": ["S_TASK_1_T3", "E_TASK_1_T3"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 1, "id": "T3", "id_e": "E_TASK_1_T3", "id_s": "S_TASK_1_T3", "inputs": ["Flow_1t58voa"], "name": "Accept Cash (T3) ", "nodeName": "bpmn2:userTask", "outputs": ["Flow_1h69eb3"], "taskNumber": 2 }, "T4": { "cstnuEdgeIds": ["S_TASK_2_T4-E_TASK_2_T4", "E_TASK_2_T4-S_TASK_2_T4"], "cstnuNodeIds": ["S_TASK_2_T4", "E_TASK_2_T4"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 2, "id": "T4", "id_e": "E_TASK_2_T4", "id_s": "S_TASK_2_T4", "inputs": ["Flow_1okivbo"], "name": "Deliver Package (T4) ", "nodeName": "bpmn2:userTask", "outputs": ["Flow_0pp9mni"], "taskNumber": 3 }, "arrows": { "Flow_0aync76": { "cstnuEdgeIds": ["E_TASK_4_T1-S_XOR_2_G1", "S_XOR_2_G1-E_TASK_4_T1"], "edgeType": "normal", "id": "Flow_0aync76", "presentInBPMN": true, "source": "T1", "target": "G1" }, "Flow_0pp9mni": { "cstnuEdgeIds": ["Ω-E_TASK_2_T4"], "edgeType": "normal", "id": "Flow_0pp9mni", "presentInBPMN": true, "source": "T4", "target": "e" }, "Flow_0wuk23o": { "cstnuEdgeIds": ["S_XOR_1_G2-E_TASK_3_IE1"], "edgeType": "normal", "id": "Flow_0wuk23o", "presentInBPMN": true, "source": "IE1", "target": "G2" }, "Flow_19suy4z": { "cstnuEdgeIds": ["E_XOR_2_G1-S_TASK_5_T2", "S_TASK_5_T2-E_XOR_2_G1"], "edgeType": "normal", "id": "Flow_19suy4z", "presentInBPMN": true, "source": "G1", "target": "T2" }, "Flow_1h69eb3": { "cstnuEdgeIds": ["S_XOR_1_G2-E_TASK_1_T3"], "edgeType": "normal", "id": "Flow_1h69eb3", "presentInBPMN": true, "source": "T3", "target": "G2" }, "Flow_1okivbo": { "cstnuEdgeIds": ["S_TASK_2_T4-E_XOR_1_G2"], "edgeType": "normal", "id": "Flow_1okivbo", "presentInBPMN": true, "source": "G2", "target": "T4" }, "Flow_1qye66p": { "cstnuEdgeIds": ["S_TASK_3_IE1-E_TASK_5_T2"], "edgeType": "normal", "id": "Flow_1qye66p", "presentInBPMN": true, "source": "T2", "target": "IE1" }, "Flow_1t58voa": { "cstnuEdgeIds": ["E_XOR_2_G1-S_TASK_1_T3", "S_TASK_1_T3-E_XOR_2_G1"], "edgeType": "normal", "id": "Flow_1t58voa", "presentInBPMN": true, "source": "G1", "target": "T3" }, "Flow_1y1s5dn": { "cstnuEdgeIds": ["S_TASK_4_T1-Z"], "edgeType": "normal", "id": "Flow_1y1s5dn", "presentInBPMN": true, "source": "StartEvent_1", "target": "T1" } }, "e": { "cstnuNodeIds": ["Ω"], "edgeType": "normal", "elementType": "END", "elementTypeNumber": "", "id": "e", "idCSTNU": "END_", "id_node": "Ω", "inputs": ["Flow_0pp9mni"], "name": "e ", "nodeName": "bpmn2:endEvent", "outputs": [], "taskNumber": 9 }, "edges_ids": { "E_TASK_1_T3-S_TASK_1_T3": { "elementIds": ["T3"], "occurrences": 1 }, "E_TASK_1_T3-S_XOR_1_G2": { "elementIds": ["Flow_1h69eb3"], "occurrences": 1 }, "E_TASK_2_T4-S_TASK_2_T4": { "elementIds": ["T4"], "occurrences": 1 }, "E_TASK_2_T4-Ω": { "elementIds": ["Flow_0pp9mni"], "occurrences": 1 }, "E_TASK_3_IE1-S_TASK_3_IE1": { "elementIds": ["IE1"], "occurrences": 1 }, "E_TASK_3_IE1-S_XOR_1_G2": { "elementIds": ["Flow_0wuk23o"], "occurrences": 1 }, "E_TASK_4_T1-S_TASK_2_T4-RelativeConstraint_2v93": { "elementIds": ["RelativeConstraint_2v93"], "occurrences": 1 }, "E_TASK_4_T1-S_TASK_4_T1": { "elementIds": ["T1"], "occurrences": 1 }, "E_TASK_4_T1-S_XOR_2_G1": { "elementIds": ["Flow_0aync76"], "occurrences": 1 }, "E_TASK_5_T2-S_TASK_3_IE1": { "elementIds": ["Flow_1qye66p"], "occurrences": 1 }, "E_TASK_5_T2-S_TASK_5_T2": { "elementIds": ["T2"], "occurrences": 1 }, "E_XOR_1_G2-S_TASK_2_T4": { "elementIds": ["Flow_1okivbo"], "occurrences": 1 }, "E_XOR_1_G2-S_XOR_1_G2": { "elementIds": ["G2"], "occurrences": 1 }, "E_XOR_2_G1-S_TASK_1_T3": { "elementIds": ["Flow_1t58voa"], "occurrences": 1 }, "E_XOR_2_G1-S_TASK_5_T2": { "elementIds": ["Flow_19suy4z"], "occurrences": 1 }, "E_XOR_2_G1-S_XOR_2_G1": { "elementIds": ["G1"], "occurrences": 1 }, "S_TASK_1_T3-E_TASK_1_T3": { "elementIds": ["T3"], "occurrences": 1 }, "S_TASK_1_T3-E_XOR_2_G1": { "elementIds": ["Flow_1t58voa"], "occurrences": 1 }, "S_TASK_2_T4-E_TASK_2_T4": { "elementIds": ["T4"], "occurrences": 1 }, "S_TASK_2_T4-E_TASK_4_T1-RelativeConstraint_2v93": { "elementIds": ["RelativeConstraint_2v93"], "occurrences": 1 }, "S_TASK_2_T4-E_XOR_1_G2": { "elementIds": ["Flow_1okivbo"], "occurrences": 1 }, "S_TASK_3_IE1-E_TASK_3_IE1": { "elementIds": ["IE1"], "occurrences": 1 }, "S_TASK_3_IE1-E_TASK_5_T2": { "elementIds": ["Flow_1qye66p"], "occurrences": 1 }, "S_TASK_4_T1-E_TASK_4_T1": { "elementIds": ["T1"], "occurrences": 1 }, "S_TASK_4_T1-Z": { "elementIds": ["Flow_1y1s5dn"], "occurrences": 1 }, "S_TASK_5_T2-E_TASK_5_T2": { "elementIds": ["T2"], "occurrences": 1 }, "S_TASK_5_T2-E_XOR_2_G1": { "elementIds": ["Flow_19suy4z"], "occurrences": 1 }, "S_XOR_1_G2-E_TASK_1_T3": { "elementIds": ["Flow_1h69eb3"], "occurrences": 1 }, "S_XOR_1_G2-E_TASK_3_IE1": { "elementIds": ["Flow_0wuk23o"], "occurrences": 1 }, "S_XOR_1_G2-E_XOR_1_G2": { "elementIds": ["G2"], "occurrences": 1 }, "S_XOR_2_G1-E_TASK_4_T1": { "elementIds": ["Flow_0aync76"], "occurrences": 1 }, "S_XOR_2_G1-E_XOR_2_G1": { "elementIds": ["G1"], "occurrences": 1 }, "Z-S_TASK_4_T1": { "elementIds": ["Flow_1y1s5dn"], "occurrences": 1 }, "Ω-E_TASK_2_T4": { "elementIds": ["Flow_0pp9mni"], "occurrences": 1 } }, "nodeObservation": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E"], "relative": { "datainput": [], "dataoutput": [], "idElementsWithRelativeConstraints": ["T1"] } });
  expect(textMessage).toStrictEqual('');
});

test('Test bpmn2cstnu of xor_bei_xor_tc', () => {
  let { diagram, xmlDoc, diagram_translated } = getDiagramToTest("xor_bei_xor_tc");
  let { xmlString, myLogObj, countObjs, myObjs, textMessage } = bpmn2cstnu(diagram, xmlDoc);
  expect(xmlString).toStrictEqual(diagram_translated);
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:userTask A  (A)  
   bpmn2:userTask A  (A)  minDuration 1 
   bpmn2:userTask A  (A)  maxDuration 2 
   bpmn2:exclusiveGateway G3  (G3)  
   bpmn2:exclusiveGateway G3  (G3)  minDuration 1 
   bpmn2:exclusiveGateway G3  (G3)  maxDuration 2 
   bpmn2:userTask BP  (BP)  
   bpmn2:userTask BP  (BP)  minDuration 1 
   bpmn2:userTask BP  (BP)  maxDuration 2 
   bpmn2:userTask C  (C)  
   bpmn2:userTask C  (C)  minDuration 1 
   bpmn2:userTask C  (C)  maxDuration 2 
   bpmn2:exclusiveGateway G2  (G2)  
   bpmn2:exclusiveGateway G2  (G2)  minDuration 1 
   bpmn2:exclusiveGateway G2  (G2)  maxDuration 2 
   bpmn2:exclusiveGateway G1  (G1)  
   bpmn2:exclusiveGateway G1  (G1)  minDuration 1 
   bpmn2:exclusiveGateway G1  (G1)  maxDuration 2 
   bpmn2:boundaryEvent B  (BoundaryEvent_01)  
   bpmn2:boundaryEvent B  (BoundaryEvent_01)  minDuration 0 
   bpmn2:boundaryEvent B  (BoundaryEvent_01)  maxDuration 1 
   bpmn2:sequenceFlow 
  [Flow_1vrtr88]  
   bpmn2:sequenceFlow 
  [Flow_1vrtr88]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_1vrtr88]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_117eefi]  
   bpmn2:sequenceFlow 
  [Flow_117eefi]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_117eefi]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_16hk73t]  
   bpmn2:sequenceFlow 
  [Flow_16hk73t]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_16hk73t]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_0orygky]  
   bpmn2:sequenceFlow 
  [Flow_0orygky]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_0orygky]  maxDuration Infinity 
   bpmn2:sequenceFlow False 
  [Flow_0dn59i6]  
   bpmn2:sequenceFlow False 
  [Flow_0dn59i6]  minDuration 0 
   bpmn2:sequenceFlow False 
  [Flow_0dn59i6]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_0jpqrq5]  
   bpmn2:sequenceFlow 
  [Flow_0jpqrq5]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_0jpqrq5]  maxDuration Infinity 
   bpmn2:sequenceFlow True 
  [Flow_1eyqzbq]  
   bpmn2:sequenceFlow True 
  [Flow_1eyqzbq]  minDuration 0 
   bpmn2:sequenceFlow True 
  [Flow_1eyqzbq]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_1890n2z]  
   bpmn2:sequenceFlow 
  [Flow_1890n2z]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_1890n2z]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_1jhny4o]  
   bpmn2:sequenceFlow 
  [Flow_1jhny4o]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_1jhny4o]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_1vrtr88]  
   bpmn2:sequenceFlow 
  [Flow_1vrtr88]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_1vrtr88]  maxDuration Infinity`, errors: "Elements with error: 0", warnings: "Elements with warning: 0"
  }));
  expect(countObjs).toStrictEqual({
    "BOUNDARY": 1, "TASK": 3, "XOR": 3, "boundaryEvents": 0,
    "boundaryEventsTotal": 1, "edges": 25, "elementsWithError": 0, "elementsWithWarning": 0,
    "endEvents": 0, "endEventsTotal": 1, "nContingents": 3, "nObservedProposition": 2,
    "nodes": 16, "startEvents": 0, "startEventsTotal": 1, "tasks": 9
  });
  // TODO: Next line creates the error: SecurityError: localStorage is not available for opaque origins,  check why  
  // expect(myObjs).toStrictEqual({"A": {"cstnuEdgeIds": ["S_TASK_1_A-E_TASK_1_A", "E_TASK_1_A-S_TASK_1_A"], "cstnuNodeIds": ["S_TASK_1_A", "E_TASK_1_A"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 1, "id": "A", "id_e": "E_TASK_1_A", "id_s": "S_TASK_1_A", "inputs": ["Flow_1eyqzbq"], "name": "A ", "nodeName": "bpmn2:userTask", "outputs": ["Flow_1vrtr88", "BoundaryEvent_01_arrow"], "taskNumber": 1}, "BP": {"cstnuEdgeIds": ["S_TASK_2_BP-E_TASK_2_BP", "E_TASK_2_BP-S_TASK_2_BP"], "cstnuNodeIds": ["S_TASK_2_BP", "E_TASK_2_BP"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 2, "id": "BP", "id_e": "E_TASK_2_BP", "id_s": "S_TASK_2_BP", "inputs": ["Flow_117eefi"], "name": "BP ", "nodeName": "bpmn2:userTask", "outputs": ["Flow_16hk73t"], "taskNumber": 3}, "BoundaryEvent_01": {"cstnuEdgeIds": ["S_BOUNDARY_1_BoundaryEvent_01-E_BOUNDARY_1_BoundaryEvent_01", "E_BOUNDARY_1_BoundaryEvent_01-S_BOUNDARY_1_BoundaryEvent_01"], "cstnuNodeIds": ["S_BOUNDARY_1_BoundaryEvent_01", "E_BOUNDARY_1_BoundaryEvent_01"], "edgeType": "normal", "elementType": "BOUNDARY", "elementTypeNumber": 1, "id": "BoundaryEvent_01", "id_e": "E_BOUNDARY_1_BoundaryEvent_01", "id_s": "S_BOUNDARY_1_BoundaryEvent_01", "inputs": ["BoundaryEvent_01_arrow"], "name": "B ", "nodeName": "bpmn2:boundaryEvent", "obs": "boundaryEvent", "observedProposition": "a", "outputs": ["Flow_117eefi"], "taskNumber": 8}, "C": {"cstnuEdgeIds": ["S_TASK_3_C-E_TASK_3_C", "E_TASK_3_C-S_TASK_3_C"], "cstnuNodeIds": ["S_TASK_3_C", "E_TASK_3_C"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 3, "id": "C", "id_e": "E_TASK_3_C", "id_s": "S_TASK_3_C", "inputs": ["Flow_0dn59i6"], "name": "C ", "nodeName": "bpmn2:userTask", "outputs": ["Flow_0orygky"], "taskNumber": 4}, "Event_0tmwunq": {"cstnuNodeIds": ["Ω"], "edgeType": "normal", "elementType": "END", "elementTypeNumber": "", "id": "Event_0tmwunq", "idCSTNU": "END_", "id_node": "Ω", "inputs": ["Flow_1jhny4o"], "name": "", "nodeName": "bpmn2:endEvent", "outputs": [], "taskNumber": 9}, "G1": {"cstnuEdgeIds": ["S_XOR_3_G1-E_XOR_3_G1", "E_XOR_3_G1-S_XOR_3_G1"], "cstnuNodeIds": ["S_XOR_3_G1", "E_XOR_3_G1"], "edgeType": "normal", "elementType": "XOR", "elementTypeNumber": 3, "id": "G1", "id_e": "E_XOR_3_G1", "id_s": "S_XOR_3_G1", "inputs": ["Flow_0jpqrq5"], "name": "G1 ", "nodeName": "bpmn2:exclusiveGateway", "obs": "split", "observedProposition": "A", "outputs": ["Flow_0dn59i6", "Flow_1eyqzbq"], "taskNumber": 6}, "G2": {"cstnuEdgeIds": ["S_XOR_2_G2-E_XOR_2_G2", "E_XOR_2_G2-S_XOR_2_G2"], "cstnuNodeIds": ["S_XOR_2_G2", "E_XOR_2_G2"], "edgeType": "normal", "elementType": "XOR", "elementTypeNumber": 2, "id": "G2", "id_e": "E_XOR_2_G2", "id_s": "S_XOR_2_G2", "inputs": ["Flow_0orygky", "Flow_1890n2z"], "name": "G2 ", "nodeName": "bpmn2:exclusiveGateway", "obs": "join", "outputs": ["Flow_1jhny4o"], "taskNumber": 5}, "G3": {"cstnuEdgeIds": ["S_XOR_1_G3-E_XOR_1_G3", "E_XOR_1_G3-S_XOR_1_G3"], "cstnuNodeIds": ["S_XOR_1_G3", "E_XOR_1_G3"], "edgeType": "normal", "elementType": "XOR", "elementTypeNumber": 1, "id": "G3", "id_e": "E_XOR_1_G3", "id_s": "S_XOR_1_G3", "inputs": ["Flow_1vrtr88", "Flow_16hk73t"], "name": "G3 ", "nodeName": "bpmn2:exclusiveGateway", "obs": "join", "outputs": ["Flow_1890n2z"], "taskNumber": 2}, "StartEvent_1": {"cstnuNodeIds": ["Z"], "edgeType": "normal", "elementType": "START", "elementTypeNumber": "", "id": "StartEvent_1", "idCSTNU": "START_", "id_node": "Z", "inputs": [], "name": "", "nodeName": "bpmn2:startEvent", "outputs": ["Flow_0jpqrq5"], "taskNumber": 7}, "arrows": {"BoundaryEvent_01_arrow": {"cstnuEdgeIds": ["S_TASK_1_A-S_BOUNDARY_1_BoundaryEvent_01", "S_BOUNDARY_1_BoundaryEvent_01-S_TASK_1_A"], "edgeType": "normal", "id": "BoundaryEvent_01_arrow", "presentInBPMN": false, "source": "A", "target": "BoundaryEvent_01"}, "Flow_0dn59i6": {"cstnuEdgeIds": ["S_TASK_3_C-E_XOR_3_G1"], "edgeType": "normal", "id": "Flow_0dn59i6", "presentInBPMN": true, "source": "G1", "target": "C"}, "Flow_0jpqrq5": {"cstnuEdgeIds": ["S_XOR_3_G1-Z"], "edgeType": "normal", "id": "Flow_0jpqrq5", "presentInBPMN": true, "source": "StartEvent_1", "target": "G1"}, "Flow_0orygky": {"cstnuEdgeIds": ["S_XOR_2_G2-E_TASK_3_C"], "edgeType": "normal", "id": "Flow_0orygky", "presentInBPMN": true, "source": "C", "target": "G2"}, "Flow_117eefi": {"cstnuEdgeIds": ["S_TASK_2_BP-E_BOUNDARY_1_BoundaryEvent_01"], "edgeType": "normal", "id": "Flow_117eefi", "presentInBPMN": true, "source": "BoundaryEvent_01", "target": "BP"}, "Flow_16hk73t": {"cstnuEdgeIds": ["S_XOR_1_G3-E_TASK_2_BP"], "edgeType": "normal", "id": "Flow_16hk73t", "presentInBPMN": true, "source": "BP", "target": "G3"}, "Flow_1890n2z": {"cstnuEdgeIds": ["S_XOR_2_G2-E_XOR_1_G3"], "edgeType": "normal", "id": "Flow_1890n2z", "presentInBPMN": true, "source": "G3", "target": "G2"}, "Flow_1eyqzbq": {"cstnuEdgeIds": ["S_TASK_1_A-E_XOR_3_G1"], "edgeType": "normal", "id": "Flow_1eyqzbq", "presentInBPMN": true, "source": "G1", "target": "A"}, "Flow_1jhny4o": {"cstnuEdgeIds": ["Ω-E_XOR_2_G2"], "edgeType": "normal", "id": "Flow_1jhny4o", "presentInBPMN": true, "source": "G2", "target": "Event_0tmwunq"}, "Flow_1vrtr88": {"cstnuEdgeIds": ["S_XOR_1_G3-E_TASK_1_A"], "edgeType": "normal", "id": "Flow_1vrtr88", "presentInBPMN": true, "source": "A", "target": "G3"}}, "edges_ids": {"E_BOUNDARY_1_BoundaryEvent_01-S_BOUNDARY_1_BoundaryEvent_01": {"elementIds": ["BoundaryEvent_01"], "occurrences": 1}, "E_BOUNDARY_1_BoundaryEvent_01-S_TASK_2_BP": {"elementIds": ["Flow_117eefi"], "occurrences": 1}, "E_TASK_1_A-S_TASK_1_A": {"elementIds": ["A"], "occurrences": 1}, "E_TASK_1_A-S_XOR_1_G3": {"elementIds": ["Flow_1vrtr88"], "occurrences": 1}, "E_TASK_2_BP-S_TASK_2_BP": {"elementIds": ["BP"], "occurrences": 1}, "E_TASK_2_BP-S_XOR_1_G3": {"elementIds": ["Flow_16hk73t"], "occurrences": 1}, "E_TASK_3_C-S_TASK_3_C": {"elementIds": ["C"], "occurrences": 1}, "E_TASK_3_C-S_XOR_2_G2": {"elementIds": ["Flow_0orygky"], "occurrences": 1}, "E_XOR_1_G3-S_XOR_1_G3": {"elementIds": ["G3"], "occurrences": 1}, "E_XOR_1_G3-S_XOR_2_G2": {"elementIds": ["Flow_1890n2z"], "occurrences": 1}, "E_XOR_2_G2-S_XOR_2_G2": {"elementIds": ["G2"], "occurrences": 1}, "E_XOR_2_G2-Ω": {"elementIds": ["Flow_1jhny4o"], "occurrences": 1}, "E_XOR_3_G1-S_TASK_1_A": {"elementIds": ["Flow_1eyqzbq"], "occurrences": 1}, "E_XOR_3_G1-S_TASK_3_C": {"elementIds": ["Flow_0dn59i6"], "occurrences": 1}, "E_XOR_3_G1-S_XOR_3_G1": {"elementIds": ["G1"], "occurrences": 1}, "S_BOUNDARY_1_BoundaryEvent_01-E_BOUNDARY_1_BoundaryEvent_01": {"elementIds": ["BoundaryEvent_01"], "occurrences": 1}, "S_BOUNDARY_1_BoundaryEvent_01-S_TASK_1_A": {"elementIds": ["BoundaryEvent_01"], "occurrences": 1}, "S_TASK_1_A-E_TASK_1_A": {"elementIds": ["A"], "occurrences": 1}, "S_TASK_1_A-E_XOR_3_G1": {"elementIds": ["Flow_1eyqzbq"], "occurrences": 1}, "S_TASK_1_A-S_BOUNDARY_1_BoundaryEvent_01": {"elementIds": ["BoundaryEvent_01"], "occurrences": 1}, "S_TASK_2_BP-E_BOUNDARY_1_BoundaryEvent_01": {"elementIds": ["Flow_117eefi"], "occurrences": 1}, "S_TASK_2_BP-E_TASK_2_BP": {"elementIds": ["BP"], "occurrences": 1}, "S_TASK_3_C-E_TASK_3_C": {"elementIds": ["C"], "occurrences": 1}, "S_TASK_3_C-E_XOR_3_G1": {"elementIds": ["Flow_0dn59i6"], "occurrences": 1}, "S_XOR_1_G3-E_TASK_1_A": {"elementIds": ["Flow_1vrtr88"], "occurrences": 1}, "S_XOR_1_G3-E_TASK_2_BP": {"elementIds": ["Flow_16hk73t"], "occurrences": 1}, "S_XOR_1_G3-E_XOR_1_G3": {"elementIds": ["G3"], "occurrences": 1}, "S_XOR_2_G2-E_TASK_3_C": {"elementIds": ["Flow_0orygky"], "occurrences": 1}, "S_XOR_2_G2-E_XOR_1_G3": {"elementIds": ["Flow_1890n2z"], "occurrences": 1}, "S_XOR_2_G2-E_XOR_2_G2": {"elementIds": ["G2"], "occurrences": 1}, "S_XOR_3_G1-E_XOR_3_G1": {"elementIds": ["G1"], "occurrences": 1}, "S_XOR_3_G1-Z": {"elementIds": ["Flow_0jpqrq5"], "occurrences": 1}, "Z-S_XOR_3_G1": {"elementIds": ["Flow_0jpqrq5"], "occurrences": 1}, "Ω-E_XOR_2_G2": {"elementIds": ["Flow_1jhny4o"], "occurrences": 1}}, "nodeObservation": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E"], "relative": {"datainput": [], "dataoutput": [], "idElementsWithRelativeConstraints": [{}]}});
  expect(textMessage).toStrictEqual('');
});

test('Test bpmn2cstnu of xor_beni_xor_tc', () => {
  let { diagram, xmlDoc, diagram_translated } = getDiagramToTest("xor_beni_xor_tc");
  let { xmlString, myLogObj, countObjs, myObjs, textMessage } = bpmn2cstnu(diagram, xmlDoc);
  expect(xmlString).toStrictEqual(diagram_translated);
  expect(trimDictionary(myLogObj)).toStrictEqual(trimDictionary({
    log: `bpmn2:userTask A  (A)  
   bpmn2:userTask A  (A)  minDuration 1 
   bpmn2:userTask A  (A)  maxDuration 2 
   bpmn2:userTask BP  (BP)  
   bpmn2:userTask BP  (BP)  minDuration 1 
   bpmn2:userTask BP  (BP)  maxDuration 2 
   bpmn2:exclusiveGateway G1  (G1)  
   bpmn2:exclusiveGateway G1  (G1)  minDuration 1 
   bpmn2:exclusiveGateway G1  (G1)  maxDuration 2 
   bpmn2:exclusiveGateway G2  (G2)  
   bpmn2:exclusiveGateway G2  (G2)  minDuration 1 
   bpmn2:exclusiveGateway G2  (G2)  maxDuration 2 
   bpmn2:userTask C  (C)  
   bpmn2:userTask C  (C)  minDuration 1 
   bpmn2:userTask C  (C)  maxDuration 2 
   bpmn2:userTask D  (D)  
   bpmn2:userTask D  (D)  minDuration 1 
   bpmn2:userTask D  (D)  maxDuration 2 
   bpmn2:boundaryEvent B  (BoundaryEvent_01)  
   bpmn2:boundaryEvent B  (BoundaryEvent_01)  minDuration 0 
   bpmn2:boundaryEvent B  (BoundaryEvent_01)  maxDuration 1 
   bpmn2:sequenceFlow 
  [Flow_117eefi]  
   bpmn2:sequenceFlow 
  [Flow_117eefi]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_117eefi]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_0jpqrq5]  
   bpmn2:sequenceFlow 
  [Flow_0jpqrq5]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_0jpqrq5]  maxDuration Infinity 
   bpmn2:sequenceFlow False 
  [Flow_0dn59i6]  
   bpmn2:sequenceFlow False 
  [Flow_0dn59i6]  minDuration 0 
   bpmn2:sequenceFlow False 
  [Flow_0dn59i6]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_0orygky]  
   bpmn2:sequenceFlow 
  [Flow_0orygky]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_0orygky]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_1s2ui2r]  
   bpmn2:sequenceFlow 
  [Flow_1s2ui2r]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_1s2ui2r]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_0rl69wh]  
   bpmn2:sequenceFlow 
  [Flow_0rl69wh]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_0rl69wh]  maxDuration Infinity 
   bpmn2:sequenceFlow True 
  [Flow_020mc4x]  
   bpmn2:sequenceFlow True 
  [Flow_020mc4x]  minDuration 0 
   bpmn2:sequenceFlow True 
  [Flow_020mc4x]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_0bn8l1y]  
   bpmn2:sequenceFlow 
  [Flow_0bn8l1y]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_0bn8l1y]  maxDuration Infinity 
   bpmn2:sequenceFlow 
  [Flow_1532vbc]  
   bpmn2:sequenceFlow 
  [Flow_1532vbc]  minDuration 0 
   bpmn2:sequenceFlow 
  [Flow_1532vbc]  maxDuration Infinity`, errors: "Elements with error: 0", warnings: "Elements with warning: 0"
  }));
  expect(countObjs).toStrictEqual({ "BOUNDARY": 1, "TASK": 4, "XOR": 2, "boundaryEvents": 0, "boundaryEventsTotal": 1, "edges": 28, "elementsWithError": 0, "elementsWithWarning": 0, "endEvents": 2, "endEventsTotal": 2, "nContingents": 4, "nObservedProposition": 2, "nodes": 18, "startEvents": 0, "startEventsTotal": 1, "tasks": 10 });
  // TODO: Next line creates the error: SecurityError: localStorage is not available for opaque origins,  check why  
  // let r1 = { "A": { "cstnuEdgeIds": ["S_TASK_1_A-E_TASK_1_A", "E_TASK_1_A-S_TASK_1_A"], "cstnuNodeIds": ["S_TASK_1_A", "E_TASK_1_A"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 1, "id": "A", "id_e": "E_TASK_1_A", "id_s": "S_TASK_1_A", "inputs": ["Flow_020mc4x"], "name": "A ", "nodeName": "bpmn2:userTask", "outputs": ["Flow_0rl69wh", "BoundaryEvent_01_arrow"], "taskNumber": 1 }, "BP": { "cstnuEdgeIds": ["S_TASK_2_BP-E_TASK_2_BP", "E_TASK_2_BP-S_TASK_2_BP"], "cstnuNodeIds": ["S_TASK_2_BP", "E_TASK_2_BP"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 2, "id": "BP", "id_e": "E_TASK_2_BP", "id_s": "S_TASK_2_BP", "inputs": ["Flow_117eefi"], "name": "BP ", "nodeName": "bpmn2:userTask", "outputs": ["Flow_1s2ui2r"], "taskNumber": 2 }, "BoundaryEvent_01": { "cstnuEdgeIds": ["S_BOUNDARY_1_BoundaryEvent_01-E_BOUNDARY_1_BoundaryEvent_01", "E_BOUNDARY_1_BoundaryEvent_01-S_BOUNDARY_1_BoundaryEvent_01"], "cstnuNodeIds": ["S_BOUNDARY_1_BoundaryEvent_01", "E_BOUNDARY_1_BoundaryEvent_01"], "edgeType": "normal", "elementType": "BOUNDARY", "elementTypeNumber": 1, "id": "BoundaryEvent_01", "id_e": "E_BOUNDARY_1_BoundaryEvent_01", "id_s": "S_BOUNDARY_1_BoundaryEvent_01", "inputs": ["BoundaryEvent_01_arrow"], "name": "B ", "nodeName": "bpmn2:boundaryEvent", "obs": "boundaryEvent", "observedProposition": "a", "outputs": ["Flow_117eefi"], "taskNumber": 10 }, "C": { "cstnuEdgeIds": ["S_TASK_3_C-E_TASK_3_C", "E_TASK_3_C-S_TASK_3_C"], "cstnuNodeIds": ["S_TASK_3_C", "E_TASK_3_C"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 3, "id": "C", "id_e": "E_TASK_3_C", "id_s": "S_TASK_3_C", "inputs": ["Flow_0dn59i6"], "name": "C ", "nodeName": "bpmn2:userTask", "outputs": ["Flow_0orygky"], "taskNumber": 5 }, "D": { "cstnuEdgeIds": ["S_TASK_4_D-E_TASK_4_D", "E_TASK_4_D-S_TASK_4_D"], "cstnuNodeIds": ["S_TASK_4_D", "E_TASK_4_D"], "edgeType": "contingent", "elementType": "TASK", "elementTypeNumber": 4, "id": "D", "id_e": "E_TASK_4_D", "id_s": "S_TASK_4_D", "inputs": ["Flow_0rl69wh"], "name": "D ", "nodeName": "bpmn2:userTask", "outputs": ["Flow_0bn8l1y"], "taskNumber": 6 }, "Event_150yops": { "cstnuNodeIds": ["Ω1"], "edgeType": "normal", "elementType": "END", "elementTypeNumber": "1", "id": "Event_150yops", "idCSTNU": "END_1", "id_node": "Ω1", "inputs": ["Flow_1s2ui2r"], "name": "", "nodeName": "bpmn2:endEvent", "outputs": [], "taskNumber": 9 }, "Event_18q35wv": { "cstnuNodeIds": ["Ω"], "edgeType": "normal", "elementType": "END", "elementTypeNumber": "0", "id": "Event_18q35wv", "idCSTNU": "END_0", "id_node": "Ω", "inputs": ["Flow_1532vbc"], "name": "", "nodeName": "bpmn2:endEvent", "outputs": [], "taskNumber": 8 }, "G1": { "cstnuEdgeIds": ["S_XOR_1_G1-E_XOR_1_G1", "E_XOR_1_G1-S_XOR_1_G1"], "cstnuNodeIds": ["S_XOR_1_G1", "E_XOR_1_G1"], "edgeType": "normal", "elementType": "XOR", "elementTypeNumber": 1, "id": "G1", "id_e": "E_XOR_1_G1", "id_s": "S_XOR_1_G1", "inputs": ["Flow_0jpqrq5"], "name": "G1 ", "nodeName": "bpmn2:exclusiveGateway", "obs": "split", "observedProposition": "A", "outputs": ["Flow_0dn59i6", "Flow_020mc4x"], "taskNumber": 3 }, "G2": { "cstnuEdgeIds": ["S_XOR_2_G2-E_XOR_2_G2", "E_XOR_2_G2-S_XOR_2_G2"], "cstnuNodeIds": ["S_XOR_2_G2", "E_XOR_2_G2"], "edgeType": "normal", "elementType": "XOR", "elementTypeNumber": 2, "id": "G2", "id_e": "E_XOR_2_G2", "id_s": "S_XOR_2_G2", "inputs": ["Flow_0orygky", "Flow_0bn8l1y"], "name": "G2 ", "nodeName": "bpmn2:exclusiveGateway", "obs": "join", "outputs": ["Flow_1532vbc"], "taskNumber": 4 }, "StartEvent_1": { "cstnuNodeIds": ["Z"], "edgeType": "normal", "elementType": "START", "elementTypeNumber": "", "id": "StartEvent_1", "idCSTNU": "START_", "id_node": "Z", "inputs": [], "name": "", "nodeName": "bpmn2:startEvent", "outputs": ["Flow_0jpqrq5"], "taskNumber": 7 }, "arrows": { "BoundaryEvent_01_arrow": { "cstnuEdgeIds": ["S_TASK_1_A-S_BOUNDARY_1_BoundaryEvent_01", "S_BOUNDARY_1_BoundaryEvent_01-S_TASK_1_A"], "edgeType": "normal", "id": "BoundaryEvent_01_arrow", "presentInBPMN": false, "source": "A", "target": "BoundaryEvent_01" }, "Flow_020mc4x": { "cstnuEdgeIds": ["S_TASK_1_A-E_XOR_1_G1"], "edgeType": "normal", "id": "Flow_020mc4x", "presentInBPMN": true, "source": "G1", "target": "A" }, "Flow_0bn8l1y": { "cstnuEdgeIds": ["S_XOR_2_G2-E_TASK_4_D"], "edgeType": "normal", "id": "Flow_0bn8l1y", "presentInBPMN": true, "source": "D", "target": "G2" }, "Flow_0dn59i6": { "cstnuEdgeIds": ["S_TASK_3_C-E_XOR_1_G1"], "edgeType": "normal", "id": "Flow_0dn59i6", "presentInBPMN": true, "source": "G1", "target": "C" }, "Flow_0jpqrq5": { "cstnuEdgeIds": ["S_XOR_1_G1-Z"], "edgeType": "normal", "id": "Flow_0jpqrq5", "presentInBPMN": true, "source": "StartEvent_1", "target": "G1" }, "Flow_0orygky": { "cstnuEdgeIds": ["S_XOR_2_G2-E_TASK_3_C"], "edgeType": "normal", "id": "Flow_0orygky", "presentInBPMN": true, "source": "C", "target": "G2" }, "Flow_0rl69wh": { "cstnuEdgeIds": ["S_TASK_4_D-E_TASK_1_A"], "edgeType": "normal", "id": "Flow_0rl69wh", "presentInBPMN": true, "source": "A", "target": "D" }, "Flow_117eefi": { "cstnuEdgeIds": ["S_TASK_2_BP-E_BOUNDARY_1_BoundaryEvent_01"], "edgeType": "normal", "id": "Flow_117eefi", "presentInBPMN": true, "source": "BoundaryEvent_01", "target": "BP" }, "Flow_1532vbc": { "cstnuEdgeIds": ["Ω-E_XOR_2_G2"], "edgeType": "normal", "id": "Flow_1532vbc", "presentInBPMN": true, "source": "G2", "target": "Event_18q35wv" }, "Flow_1s2ui2r": { "cstnuEdgeIds": ["Ω1-E_TASK_2_BP"], "edgeType": "normal", "id": "Flow_1s2ui2r", "presentInBPMN": true, "source": "BP", "target": "Event_150yops" } }, "edges_ids": { "E_BOUNDARY_1_BoundaryEvent_01-S_BOUNDARY_1_BoundaryEvent_01": { "elementIds": ["BoundaryEvent_01"], "occurrences": 1 }, "E_BOUNDARY_1_BoundaryEvent_01-S_TASK_2_BP": { "elementIds": ["Flow_117eefi"], "occurrences": 1 }, "E_TASK_1_A-S_TASK_1_A": { "elementIds": ["A"], "occurrences": 1 }, "E_TASK_1_A-S_TASK_4_D": { "elementIds": ["Flow_0rl69wh"], "occurrences": 1 }, "E_TASK_2_BP-S_TASK_2_BP": { "elementIds": ["BP"], "occurrences": 1 }, "E_TASK_2_BP-Ω1": { "elementIds": ["Flow_1s2ui2r"], "occurrences": 1 }, "E_TASK_3_C-S_TASK_3_C": { "elementIds": ["C"], "occurrences": 1 }, "E_TASK_3_C-S_XOR_2_G2": { "elementIds": ["Flow_0orygky"], "occurrences": 1 }, "E_TASK_4_D-S_TASK_4_D": { "elementIds": ["D"], "occurrences": 1 }, "E_TASK_4_D-S_XOR_2_G2": { "elementIds": ["Flow_0bn8l1y"], "occurrences": 1 }, "E_XOR_1_G1-S_TASK_1_A": { "elementIds": ["Flow_020mc4x"], "occurrences": 1 }, "E_XOR_1_G1-S_TASK_3_C": { "elementIds": ["Flow_0dn59i6"], "occurrences": 1 }, "E_XOR_1_G1-S_XOR_1_G1": { "elementIds": ["G1"], "occurrences": 1 }, "E_XOR_2_G2-S_XOR_2_G2": { "elementIds": ["G2"], "occurrences": 1 }, "E_XOR_2_G2-Ω": { "elementIds": ["Flow_1532vbc"], "occurrences": 1 }, "S_BOUNDARY_1_BoundaryEvent_01-E_BOUNDARY_1_BoundaryEvent_01": { "elementIds": ["BoundaryEvent_01"], "occurrences": 1 }, "S_BOUNDARY_1_BoundaryEvent_01-S_TASK_1_A": { "elementIds": ["BoundaryEvent_01"], "occurrences": 1 }, "S_TASK_1_A-E_TASK_1_A": { "elementIds": ["A"], "occurrences": 1 }, "S_TASK_1_A-E_XOR_1_G1": { "elementIds": ["Flow_020mc4x"], "occurrences": 1 }, "S_TASK_1_A-S_BOUNDARY_1_BoundaryEvent_01": { "elementIds": ["BoundaryEvent_01"], "occurrences": 1 }, "S_TASK_2_BP-E_BOUNDARY_1_BoundaryEvent_01": { "elementIds": ["Flow_117eefi"], "occurrences": 1 }, "S_TASK_2_BP-E_TASK_2_BP": { "elementIds": ["BP"], "occurrences": 1 }, "S_TASK_3_C-E_TASK_3_C": { "elementIds": ["C"], "occurrences": 1 }, "S_TASK_3_C-E_XOR_1_G1": { "elementIds": ["Flow_0dn59i6"], "occurrences": 1 }, "S_TASK_4_D-E_TASK_1_A": { "elementIds": ["Flow_0rl69wh"], "occurrences": 1 }, "S_TASK_4_D-E_TASK_4_D": { "elementIds": ["D"], "occurrences": 1 }, "S_XOR_1_G1-E_XOR_1_G1": { "elementIds": ["G1"], "occurrences": 1 }, "S_XOR_1_G1-Z": { "elementIds": ["Flow_0jpqrq5"], "occurrences": 1 }, "S_XOR_2_G2-E_TASK_3_C": { "elementIds": ["Flow_0orygky"], "occurrences": 1 }, "S_XOR_2_G2-E_TASK_4_D": { "elementIds": ["Flow_0bn8l1y"], "occurrences": 1 }, "S_XOR_2_G2-E_XOR_2_G2": { "elementIds": ["G2"], "occurrences": 1 }, "Z-S_XOR_1_G1": { "elementIds": ["Flow_0jpqrq5"], "occurrences": 1 }, "Ω-E_XOR_2_G2": { "elementIds": ["Flow_1532vbc"], "occurrences": 1 }, "Ω1-E_TASK_2_BP": { "elementIds": ["Flow_1s2ui2r"], "occurrences": 1 } }, "nodeObservation": ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E"], "relative": { "datainput": [], "dataoutput": [], "idElementsWithRelativeConstraints": [{}] } };
  // expect(myObjs).toStrictEqual(r1);
  expect(textMessage).toStrictEqual('');
});

/**
 * Removes spaces, tabs, and breaklines from a string 
 * @param {String} string 
 * @returns {String} string updated
 */
function trimString(string) {
  return string.replace(/(\r\n|\n|\r|\s)/gm, "");
}

/**
 * For every element in a dictionary (Object), it removes spaces, tabs, and breaklines from a string 
 * @param {Object} dictionary 
 * @returns {Object} dictionary updated
 */
function trimDictionary(dictionary) {
  let k;
  for (k in dictionary) {
    dictionary[k] = trimString(dictionary[k]);
  }
  return dictionary;
}


/**
 * Get a XML element from a BPMN document. The elements contain valid and invalid configurations to perform the tests.
 * 
 * @param {*} idElement 
 * @returns 
 */
function getExample01(idElement) {

  // Modified version of Example01.bpmn, with valid and invalid configurations
  // of minDuration and maxDiration to perform the tests
  let example01 = `<?xml version="1.0" encoding="UTF-8"?>
  <bpmn2:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:tempcon="https://gitlab.com/univr.di/TimeAwareBPMN/-/blob/main/model/TABPMN20.xsd" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="sample-diagram" targetNamespace="http://bpmn.io/schema/bpmn" xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd">
    <bpmn2:process id="Process_1" isExecutable="false">
      <bpmn2:startEvent id="StartEvent_1" name="s">
        <bpmn2:outgoing>Flow_1y1s5dn</bpmn2:outgoing>
      </bpmn2:startEvent>
      <bpmn2:exclusiveGateway id="G2" name="G2">
        <bpmn2:extensionElements>
          <tempcon:tGateway>
            <tempcon:observedProposition>R</tempcon:observedProposition>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tGateway>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_1h69eb3</bpmn2:incoming>
        <bpmn2:incoming>Flow_0wuk23o</bpmn2:incoming>
        <bpmn2:outgoing>Flow_1o81hy4</bpmn2:outgoing>
      </bpmn2:exclusiveGateway>
      <bpmn2:sequenceFlow id="Flow_1h69eb3" sourceRef="T3" targetRef="G2" />
      <bpmn2:endEvent id="e" name="e">
        <bpmn2:incoming>Flow_0pp9mni</bpmn2:incoming>
      </bpmn2:endEvent>
      <bpmn2:userTask id="T3" name="Accept Cash (T3)">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>14</tempcon:minDuration>
              <tempcon:maxDuration>6</tempcon:maxDuration>
              <tempcon:propositionalLabel>¬A</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_1t58voa</bpmn2:incoming>
        <bpmn2:outgoing>Flow_1h69eb3</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:sequenceFlow id="Flow_0wuk23o" sourceRef="IE1" targetRef="G2" />
      <bpmn2:userTask id="T4" name="Deliver Package (T4)">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>4</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_07pscvc</bpmn2:incoming>
        <bpmn2:outgoing>Flow_0pp9mni</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:sequenceFlow id="Flow_0pp9mni" sourceRef="T4" targetRef="e" />
      <bpmn2:sequenceFlow id="Flow_1qye66p" sourceRef="T2" targetRef="IE1" />
      <bpmn2:intermediateCatchEvent id="IE1" name="Payment authorized (IE1)">
        <bpmn2:extensionElements>
          <tempcon:tEvent>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>3</tempcon:maxDuration>
              <tempcon:propositionalLabel>A</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tEvent>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_1qye66p</bpmn2:incoming>
        <bpmn2:outgoing>Flow_0wuk23o</bpmn2:outgoing>
        <bpmn2:messageEventDefinition id="MessageEventDefinition_1by8qh7" />
      </bpmn2:intermediateCatchEvent>
      <bpmn2:userTask id="T1" name="Identify Payment Method (T1)">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>2</tempcon:minDuration>
              <tempcon:maxDuration>3</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
          <tempcon:relativeConstraint type="custom:connection" id_relativeConstraint="RelativeConstraint_2v93" waypoints="[{&#34;x&#34;:220,&#34;y&#34;:220},{&#34;x&#34;:220,&#34;y&#34;:360},{&#34;x&#34;:900,&#34;y&#34;:360},{&#34;x&#34;:900,&#34;y&#34;:220}]">
            <tempcon:target>T4</tempcon:target>
            <tempcon:tDuration>
              <tempcon:minDuration>2</tempcon:minDuration>
              <tempcon:maxDuration>31</tempcon:maxDuration>
            </tempcon:tDuration>
            <tempcon:from>start</tempcon:from>
            <tempcon:to>end</tempcon:to>
          </tempcon:relativeConstraint>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_1y1s5dn</bpmn2:incoming>
        <bpmn2:outgoing>Flow_0aync76</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:sequenceFlow id="Flow_1y1s5dn" sourceRef="StartEvent_1" targetRef="T1" />
      <bpmn2:userTask id="T2" name="Authorize Credit Card (T2)">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>1.1</tempcon:minDuration>
              <tempcon:maxDuration>2.2</tempcon:maxDuration>
              <tempcon:propositionalLabel>A</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_19suy4z</bpmn2:incoming>
        <bpmn2:outgoing>Flow_1qye66p</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:exclusiveGateway id="G1" name="Credit Card Payment? (G1)" updated="true">
        <bpmn2:extensionElements>
          <tempcon:tGateway>
            <tempcon:observedProposition>A</tempcon:observedProposition>
            <tempcon:tDuration>
              <tempcon:minDuration>12</tempcon:minDuration>
              <tempcon:maxDuration>4</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tGateway>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_0aync76</bpmn2:incoming>
        <bpmn2:outgoing>Flow_19suy4z</bpmn2:outgoing>
        <bpmn2:outgoing>Flow_1t58voa</bpmn2:outgoing>
      </bpmn2:exclusiveGateway>
      <bpmn2:sequenceFlow id="Flow_0aync76" sourceRef="T1" targetRef="G1">
        <bpmn2:extensionElements>
          <tempcon:tSequenceFlow>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
            </tempcon:tDuration>
          </tempcon:tSequenceFlow>
        </bpmn2:extensionElements>
      </bpmn2:sequenceFlow>
      <bpmn2:sequenceFlow id="Flow_1o81hy4" sourceRef="G2" targetRef="G3" />
      <bpmn2:userTask id="T5" name="Produce product (T5) &#10;">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>3</tempcon:minDuration>
              <tempcon:maxDuration>10</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_09nhrze</bpmn2:incoming>
        <bpmn2:outgoing>Flow_0lxpcyc</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:sequenceFlow id="Flow_09nhrze" sourceRef="G3" targetRef="T5" />
      <bpmn2:sequenceFlow id="Flow_0lxpcyc" sourceRef="T5" targetRef="G4" />
      <bpmn2:sequenceFlow id="Flow_07pscvc" sourceRef="G4" targetRef="T4" />
      <bpmn2:userTask id="T6" name="Account management (T6)&#10;">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>2</tempcon:minDuration>
              <tempcon:maxDuration>5</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_0wi2yj5</bpmn2:incoming>
        <bpmn2:outgoing>Flow_0sfophu</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:sequenceFlow id="Flow_0wi2yj5" sourceRef="G3" targetRef="T6" />
      <bpmn2:sequenceFlow id="Flow_0sfophu" sourceRef="T6" targetRef="G4" />
      <bpmn2:parallelGateway id="G3" name="G3">
        <bpmn2:extensionElements>
          <tempcon:tGateway>
            <tempcon:tDuration>
              <tempcon:minDuration>0</tempcon:minDuration>
              <tempcon:maxDuration>1</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tGateway>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_1o81hy4</bpmn2:incoming>
        <bpmn2:outgoing>Flow_09nhrze</bpmn2:outgoing>
        <bpmn2:outgoing>Flow_0wi2yj5</bpmn2:outgoing>
      </bpmn2:parallelGateway>
      <bpmn2:parallelGateway id="G4" name="G4">
        <bpmn2:extensionElements>
          <tempcon:tGateway>
            <tempcon:tDuration>
              <tempcon:minDuration>a</tempcon:minDuration>
              <tempcon:maxDuration>b</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tGateway>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_0lxpcyc</bpmn2:incoming>
        <bpmn2:incoming>Flow_0sfophu</bpmn2:incoming>
        <bpmn2:outgoing>Flow_07pscvc</bpmn2:outgoing>
      </bpmn2:parallelGateway>
      <bpmn2:sequenceFlow id="Flow_19suy4z" name="True" sourceRef="G1" targetRef="T2">
        <bpmn2:extensionElements>
          <tempcon:tSequenceFlow>
            <tempcon:isTrueBranch>true</tempcon:isTrueBranch>
            <tempcon:tDuration>
              <tempcon:minDuration>1.1</tempcon:minDuration>
              <tempcon:maxDuration>4.4</tempcon:maxDuration>
            </tempcon:tDuration>
          </tempcon:tSequenceFlow>
        </bpmn2:extensionElements>
      </bpmn2:sequenceFlow>
      <bpmn2:sequenceFlow id="Flow_1t58voa" name="False" sourceRef="G1" targetRef="T3" updated="true">
        <bpmn2:extensionElements>
          <tempcon:tSequenceFlow>
            <tempcon:isTrueBranch>false</tempcon:isTrueBranch>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>3</tempcon:maxDuration>
            </tempcon:tDuration>
          </tempcon:tSequenceFlow>
        </bpmn2:extensionElements>
      </bpmn2:sequenceFlow>
    </bpmn2:process>
    <bpmndi:BPMNDiagram id="BPMNDiagram_1">
      <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
        <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1">
          <dc:Bounds x="112" y="162" width="36" height="36" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="128" y="205" width="6" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Gateway_0iaxns3_di" bpmnElement="G2" isMarkerVisible="true">
          <dc:Bounds x="445" y="155" width="50" height="50" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="493" y="205" width="15" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_1h69eb3_di" bpmnElement="Flow_1h69eb3">
          <di:waypoint x="370" y="290" />
          <di:waypoint x="470" y="290" />
          <di:waypoint x="470" y="205" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Event_0kg2g9g_di" bpmnElement="e">
          <dc:Bounds x="1012" y="162" width="36" height="36" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="1027" y="205" width="7" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Activity_0w5uk78_di" bpmnElement="T3">
          <dc:Bounds x="270" y="250" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_0wuk23o_di" bpmnElement="Flow_0wuk23o">
          <di:waypoint x="470" y="68" />
          <di:waypoint x="470" y="155" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Activity_1q16nqf_di" bpmnElement="T4">
          <dc:Bounds x="870" y="140" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_0pp9mni_di" bpmnElement="Flow_0pp9mni">
          <di:waypoint x="970" y="180" />
          <di:waypoint x="1012" y="180" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_1qye66p_di" bpmnElement="Flow_1qye66p">
          <di:waypoint x="370" y="50" />
          <di:waypoint x="452" y="50" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Event_0061b0u_di" bpmnElement="IE1">
          <dc:Bounds x="452" y="32" width="36" height="36" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="430" y="-14" width="79" height="27" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Activity_1ihqlbg_di" bpmnElement="T1">
          <dc:Bounds x="170" y="140" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_1y1s5dn_di" bpmnElement="Flow_1y1s5dn">
          <di:waypoint x="148" y="180" />
          <di:waypoint x="170" y="180" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Activity_0te0nh1_di" bpmnElement="T2">
          <dc:Bounds x="270" y="10" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Gateway_0s4sc6g_di" bpmnElement="G1" isMarkerVisible="true">
          <dc:Bounds x="295" y="155" width="50" height="50" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="342" y="170" width="75" height="27" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_0aync76_di" bpmnElement="Flow_0aync76">
          <di:waypoint x="270" y="180" />
          <di:waypoint x="295" y="180" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_1o81hy4_di" bpmnElement="Flow_1o81hy4">
          <di:waypoint x="495" y="180" />
          <di:waypoint x="575" y="180" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Activity_19766g5_di" bpmnElement="T5">
          <dc:Bounds x="640" y="10" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_09nhrze_di" bpmnElement="Flow_09nhrze">
          <di:waypoint x="600" y="155" />
          <di:waypoint x="600" y="50" />
          <di:waypoint x="640" y="50" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_0lxpcyc_di" bpmnElement="Flow_0lxpcyc">
          <di:waypoint x="740" y="50" />
          <di:waypoint x="800" y="50" />
          <di:waypoint x="800" y="155" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_07pscvc_di" bpmnElement="Flow_07pscvc">
          <di:waypoint x="825" y="180" />
          <di:waypoint x="870" y="180" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Activity_06147cf_di" bpmnElement="T6">
          <dc:Bounds x="650" y="260" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_0wi2yj5_di" bpmnElement="Flow_0wi2yj5">
          <di:waypoint x="600" y="205" />
          <di:waypoint x="600" y="300" />
          <di:waypoint x="650" y="300" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_0sfophu_di" bpmnElement="Flow_0sfophu">
          <di:waypoint x="750" y="300" />
          <di:waypoint x="800" y="300" />
          <di:waypoint x="800" y="205" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Gateway_1672sy3_di" bpmnElement="G3">
          <dc:Bounds x="575" y="155" width="50" height="50" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="622" y="205" width="15" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Gateway_1nv66eh_di" bpmnElement="G4">
          <dc:Bounds x="775" y="155" width="50" height="50" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="812" y="205" width="15" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_19suy4z_di" bpmnElement="Flow_19suy4z">
          <di:waypoint x="320" y="155" />
          <di:waypoint x="320" y="90" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="324" y="120" width="23" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_1t58voa_di" bpmnElement="Flow_1t58voa">
          <di:waypoint x="320" y="205" />
          <di:waypoint x="320" y="250" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="322" y="225" width="27" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNEdge>
      </bpmndi:BPMNPlane>
    </bpmndi:BPMNDiagram>
  </bpmn2:definitions>`;

  const JSDOM = require("jsdom");
  // import JSDOM from "jsdom";
  // Create empty DOM, the imput param here is for HTML not XML, and we don want to parse HTML
  const dom = new JSDOM.JSDOM("");
  // Get DOMParser, same API as in browser
  const DOMParser = dom.window.DOMParser;
  const parser = new DOMParser;
  // Create document by parsing XML
  const xmlDoc = parser.parseFromString(example01, "text/xml");

  if (idElement)
    return xmlDoc.getElementById(idElement);
  else
    return xmlDoc;
}


/**
 * Function that contains different BPMN XML string to test the translation
 */
function getDiagramToTest(idDiagram) {
  // diagram_example01 corrected
  let example01 = `<?xml version="1.0" encoding="UTF-8"?>
<bpmn2:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:tempcon="https://gitlab.com/univr.di/TimeAwareBPMN/-/blob/main/model/TABPMN20.xsd" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="sample-diagram" targetNamespace="http://bpmn.io/schema/bpmn" xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd">
 <bpmn2:process id="Process_1" isExecutable="false">
   <bpmn2:startEvent id="StartEvent_1" name="s">
     <bpmn2:outgoing>Flow_1y1s5dn</bpmn2:outgoing>
   </bpmn2:startEvent>
   <bpmn2:exclusiveGateway id="G2" name="G2">
     <bpmn2:extensionElements>
       <tempcon:tGateway>
         <tempcon:observedProposition>R</tempcon:observedProposition>
         <tempcon:tDuration>
           <tempcon:minDuration>1</tempcon:minDuration>
           <tempcon:maxDuration>2</tempcon:maxDuration>
           <tempcon:propositionalLabel></tempcon:propositionalLabel>
         </tempcon:tDuration>
       </tempcon:tGateway>
     </bpmn2:extensionElements>
     <bpmn2:incoming>Flow_1h69eb3</bpmn2:incoming>
     <bpmn2:incoming>Flow_0wuk23o</bpmn2:incoming>
     <bpmn2:outgoing>Flow_1okivbo</bpmn2:outgoing>
   </bpmn2:exclusiveGateway>
   <bpmn2:sequenceFlow id="Flow_1h69eb3" sourceRef="T3" targetRef="G2" />
   <bpmn2:endEvent id="e" name="e">
     <bpmn2:incoming>Flow_0pp9mni</bpmn2:incoming>
   </bpmn2:endEvent>
   <bpmn2:userTask id="T3" name="Accept Cash (T3)">
     <bpmn2:extensionElements>
       <tempcon:tTask>
         <tempcon:tDuration>
           <tempcon:minDuration>1</tempcon:minDuration>
           <tempcon:maxDuration>6</tempcon:maxDuration>
           <tempcon:propositionalLabel>¬A</tempcon:propositionalLabel>
         </tempcon:tDuration>
       </tempcon:tTask>
     </bpmn2:extensionElements>
     <bpmn2:incoming>Flow_1t58voa</bpmn2:incoming>
     <bpmn2:outgoing>Flow_1h69eb3</bpmn2:outgoing>
   </bpmn2:userTask>
   <bpmn2:sequenceFlow id="Flow_0wuk23o" sourceRef="IE1" targetRef="G2" />
   <bpmn2:userTask id="T4" name="Deliver Package (T4)">
     <bpmn2:extensionElements>
       <tempcon:tTask>
         <tempcon:tDuration>
           <tempcon:minDuration>1</tempcon:minDuration>
           <tempcon:maxDuration>4</tempcon:maxDuration>
           <tempcon:propositionalLabel></tempcon:propositionalLabel>
         </tempcon:tDuration>
       </tempcon:tTask>
     </bpmn2:extensionElements>
     <bpmn2:incoming>Flow_1okivbo</bpmn2:incoming>
     <bpmn2:outgoing>Flow_0pp9mni</bpmn2:outgoing>
   </bpmn2:userTask>
   <bpmn2:sequenceFlow id="Flow_0pp9mni" sourceRef="T4" targetRef="e" />
   <bpmn2:sequenceFlow id="Flow_1qye66p" sourceRef="T2" targetRef="IE1" />
   <bpmn2:intermediateCatchEvent id="IE1" name="Payment authorized (IE1)">
     <bpmn2:extensionElements>
       <tempcon:tEvent>
         <tempcon:tDuration>
           <tempcon:minDuration>1</tempcon:minDuration>
           <tempcon:maxDuration>3</tempcon:maxDuration>
           <tempcon:propositionalLabel>A</tempcon:propositionalLabel>
         </tempcon:tDuration>
       </tempcon:tEvent>
     </bpmn2:extensionElements>
     <bpmn2:incoming>Flow_1qye66p</bpmn2:incoming>
     <bpmn2:outgoing>Flow_0wuk23o</bpmn2:outgoing>
     <bpmn2:messageEventDefinition id="MessageEventDefinition_1by8qh7" />
   </bpmn2:intermediateCatchEvent>
   <bpmn2:userTask id="T1" name="Identify Payment Method (T1)">
     <bpmn2:extensionElements>
       <tempcon:tTask>
         <tempcon:tDuration>
           <tempcon:minDuration>2</tempcon:minDuration>
           <tempcon:maxDuration>3</tempcon:maxDuration>
           <tempcon:propositionalLabel></tempcon:propositionalLabel>
         </tempcon:tDuration>
       </tempcon:tTask>
       <tempcon:relativeConstraint type="custom:connection" id_relativeConstraint="RelativeConstraint_2v93" waypoints="[{&#34;x&#34;:220,&#34;y&#34;:220},{&#34;x&#34;:220,&#34;y&#34;:350},{&#34;x&#34;:590,&#34;y&#34;:350},{&#34;x&#34;:590,&#34;y&#34;:220}]">
         <tempcon:target>T4</tempcon:target>
         <tempcon:tDuration>
           <tempcon:minDuration>2</tempcon:minDuration>
           <tempcon:maxDuration>15</tempcon:maxDuration>
         </tempcon:tDuration>
         <tempcon:from>end</tempcon:from>
         <tempcon:to>start</tempcon:to>
       </tempcon:relativeConstraint>
     </bpmn2:extensionElements>
     <bpmn2:incoming>Flow_1y1s5dn</bpmn2:incoming>
     <bpmn2:outgoing>Flow_0aync76</bpmn2:outgoing>
   </bpmn2:userTask>
   <bpmn2:sequenceFlow id="Flow_1y1s5dn" sourceRef="StartEvent_1" targetRef="T1" />
   <bpmn2:userTask id="T2" name="Authorize Credit Card (T2)">
     <bpmn2:extensionElements>
       <tempcon:tTask>
         <tempcon:tDuration>
           <tempcon:minDuration>1</tempcon:minDuration>
           <tempcon:maxDuration>2</tempcon:maxDuration>
           <tempcon:propositionalLabel>A</tempcon:propositionalLabel>
         </tempcon:tDuration>
       </tempcon:tTask>
     </bpmn2:extensionElements>
     <bpmn2:incoming>Flow_19suy4z</bpmn2:incoming>
     <bpmn2:outgoing>Flow_1qye66p</bpmn2:outgoing>
   </bpmn2:userTask>
   <bpmn2:exclusiveGateway id="G1" name="Credit Card Payment? (G1)" updated="true">
     <bpmn2:extensionElements>
       <tempcon:tGateway>
         <tempcon:observedProposition>A</tempcon:observedProposition>
         <tempcon:tDuration>
           <tempcon:minDuration>2</tempcon:minDuration>
           <tempcon:maxDuration>4</tempcon:maxDuration>
           <tempcon:propositionalLabel></tempcon:propositionalLabel>
         </tempcon:tDuration>
       </tempcon:tGateway>
     </bpmn2:extensionElements>
     <bpmn2:incoming>Flow_0aync76</bpmn2:incoming>
     <bpmn2:outgoing>Flow_19suy4z</bpmn2:outgoing>
     <bpmn2:outgoing>Flow_1t58voa</bpmn2:outgoing>
   </bpmn2:exclusiveGateway>
   <bpmn2:sequenceFlow id="Flow_0aync76" sourceRef="T1" targetRef="G1" updated="true">
     <bpmn2:extensionElements>
       <tempcon:tSequenceFlow>
         <tempcon:tDuration>
           <tempcon:minDuration>1</tempcon:minDuration>
           <tempcon:maxDuration>2</tempcon:maxDuration>
         </tempcon:tDuration>
       </tempcon:tSequenceFlow>
     </bpmn2:extensionElements>
   </bpmn2:sequenceFlow>
   <bpmn2:sequenceFlow id="Flow_19suy4z" name="True" sourceRef="G1" targetRef="T2">
     <bpmn2:extensionElements>
       <tempcon:tSequenceFlow>
         <tempcon:isTrueBranch>true</tempcon:isTrueBranch>
         <tempcon:tDuration>
           <tempcon:minDuration>1</tempcon:minDuration>
           <tempcon:maxDuration>4</tempcon:maxDuration>
         </tempcon:tDuration>
       </tempcon:tSequenceFlow>
     </bpmn2:extensionElements>
   </bpmn2:sequenceFlow>
   <bpmn2:sequenceFlow id="Flow_1t58voa" name="False" sourceRef="G1" targetRef="T3" updated="true">
     <bpmn2:extensionElements>
       <tempcon:tSequenceFlow>
         <tempcon:isTrueBranch>false</tempcon:isTrueBranch>
         <tempcon:tDuration>
           <tempcon:minDuration>1</tempcon:minDuration>
           <tempcon:maxDuration>3</tempcon:maxDuration>
         </tempcon:tDuration>
       </tempcon:tSequenceFlow>
     </bpmn2:extensionElements>
   </bpmn2:sequenceFlow>
   <bpmn2:sequenceFlow id="Flow_1okivbo" sourceRef="G2" targetRef="T4" />
 </bpmn2:process>
 <bpmndi:BPMNDiagram id="BPMNDiagram_1">
   <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
     <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1">
       <dc:Bounds x="112" y="162" width="36" height="36" />
       <bpmndi:BPMNLabel>
         <dc:Bounds x="128" y="205" width="6" height="14" />
       </bpmndi:BPMNLabel>
     </bpmndi:BPMNShape>
     <bpmndi:BPMNShape id="Gateway_0iaxns3_di" bpmnElement="G2" isMarkerVisible="true">
       <dc:Bounds x="445" y="155" width="50" height="50" />
       <bpmndi:BPMNLabel>
         <dc:Bounds x="493" y="205" width="15" height="14" />
       </bpmndi:BPMNLabel>
     </bpmndi:BPMNShape>
     <bpmndi:BPMNEdge id="Flow_1h69eb3_di" bpmnElement="Flow_1h69eb3">
       <di:waypoint x="370" y="290" />
       <di:waypoint x="470" y="290" />
       <di:waypoint x="470" y="205" />
     </bpmndi:BPMNEdge>
     <bpmndi:BPMNShape id="Event_0kg2g9g_di" bpmnElement="e">
       <dc:Bounds x="712" y="162" width="36" height="36" />
       <bpmndi:BPMNLabel>
         <dc:Bounds x="727" y="205" width="7" height="14" />
       </bpmndi:BPMNLabel>
     </bpmndi:BPMNShape>
     <bpmndi:BPMNShape id="Activity_0w5uk78_di" bpmnElement="T3">
       <dc:Bounds x="270" y="250" width="100" height="80" />
     </bpmndi:BPMNShape>
     <bpmndi:BPMNEdge id="Flow_0wuk23o_di" bpmnElement="Flow_0wuk23o">
       <di:waypoint x="470" y="68" />
       <di:waypoint x="470" y="155" />
     </bpmndi:BPMNEdge>
     <bpmndi:BPMNShape id="Activity_1q16nqf_di" bpmnElement="T4">
       <dc:Bounds x="560" y="140" width="100" height="80" />
     </bpmndi:BPMNShape>
     <bpmndi:BPMNEdge id="Flow_0pp9mni_di" bpmnElement="Flow_0pp9mni">
       <di:waypoint x="660" y="180" />
       <di:waypoint x="712" y="180" />
     </bpmndi:BPMNEdge>
     <bpmndi:BPMNEdge id="Flow_1qye66p_di" bpmnElement="Flow_1qye66p">
       <di:waypoint x="370" y="50" />
       <di:waypoint x="452" y="50" />
     </bpmndi:BPMNEdge>
     <bpmndi:BPMNShape id="Event_0061b0u_di" bpmnElement="IE1">
       <dc:Bounds x="452" y="32" width="36" height="36" />
       <bpmndi:BPMNLabel>
         <dc:Bounds x="430" y="-14" width="79" height="27" />
       </bpmndi:BPMNLabel>
     </bpmndi:BPMNShape>
     <bpmndi:BPMNShape id="Activity_1ihqlbg_di" bpmnElement="T1">
       <dc:Bounds x="170" y="140" width="100" height="80" />
     </bpmndi:BPMNShape>
     <bpmndi:BPMNEdge id="Flow_1y1s5dn_di" bpmnElement="Flow_1y1s5dn">
       <di:waypoint x="148" y="180" />
       <di:waypoint x="170" y="180" />
     </bpmndi:BPMNEdge>
     <bpmndi:BPMNShape id="Activity_0te0nh1_di" bpmnElement="T2">
       <dc:Bounds x="270" y="10" width="100" height="80" />
     </bpmndi:BPMNShape>
     <bpmndi:BPMNShape id="Gateway_0s4sc6g_di" bpmnElement="G1" isMarkerVisible="true">
       <dc:Bounds x="295" y="155" width="50" height="50" />
       <bpmndi:BPMNLabel>
         <dc:Bounds x="342" y="170" width="75" height="27" />
       </bpmndi:BPMNLabel>
     </bpmndi:BPMNShape>
     <bpmndi:BPMNEdge id="Flow_0aync76_di" bpmnElement="Flow_0aync76">
       <di:waypoint x="270" y="180" />
       <di:waypoint x="295" y="180" />
     </bpmndi:BPMNEdge>
     <bpmndi:BPMNEdge id="Flow_19suy4z_di" bpmnElement="Flow_19suy4z">
       <di:waypoint x="320" y="155" />
       <di:waypoint x="320" y="90" />
       <bpmndi:BPMNLabel>
         <dc:Bounds x="324" y="120" width="23" height="14" />
       </bpmndi:BPMNLabel>
     </bpmndi:BPMNEdge>
     <bpmndi:BPMNEdge id="Flow_1t58voa_di" bpmnElement="Flow_1t58voa">
       <di:waypoint x="320" y="205" />
       <di:waypoint x="320" y="250" />
       <bpmndi:BPMNLabel>
         <dc:Bounds x="322" y="225" width="27" height="14" />
       </bpmndi:BPMNLabel>
     </bpmndi:BPMNEdge>
     <bpmndi:BPMNEdge id="Flow_1okivbo_di" bpmnElement="Flow_1okivbo">
       <di:waypoint x="495" y="180" />
       <di:waypoint x="560" y="180" />
     </bpmndi:BPMNEdge>
   </bpmndi:BPMNPlane>
 </bpmndi:BPMNDiagram>
</bpmn2:definitions>`;
  let example01_translated = `<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns/graphml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns/graphml">
  <key id="nContingent" for="graph">
    <desc>Number of contingents in the graph</desc>
    <default>0</default>
  </key>
  <key id="nObservedProposition" for="graph">
    <desc>Number of observed propositions in the graph</desc>
    <default>0</default>
  </key>
  <key id="NetworkType" for="graph">
    <desc>Network Type</desc>
    <default>CSTNU</default>
  </key>
  <key id="nEdges" for="graph">
    <desc>Number of edges in the graph</desc>
    <default>0</default>
  </key>
  <key id="nVertices" for="graph">
    <desc>Number of vertices in the graph</desc>
    <default>0</default>
  </key>
  <key id="Name" for="graph">
    <desc>Graph Name</desc>
    <default/>
  </key>
  <key id="Obs" for="node">
    <desc>Proposition Observed. Value specification: [a-zA-F]</desc>
    <default/>
  </key>
  <key id="x" for="node">
    <desc>The x coordinate for the visualitation. A positive value.</desc>
    <default>0</default>
  </key>
  <key id="Label" for="node">
    <desc>Label. Format: [¬[a-zA-F]|[a-zA-F]]+|⊡</desc>
    <default>⊡</default>
  </key>
  <key id="y" for="node">
    <desc>The y coordinate for the visualitation. A positive value.</desc>
    <default>0</default>
  </key>
  <key id="Potential" for="node">
    <desc>Labeled Potential Values. Format: {[('node name (no case modification)', 'integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <key id="Type" for="edge">
    <desc>Type: Possible values: normal|contingent|constraint|derived|internal.</desc>
    <default>normal</default>
  </key>
  <key id="LowerCaseLabeledValues" for="edge">
    <desc>Labeled Lower-Case Values. Format: {[('node name (no case modification)', 'integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <key id="UpperCaseLabeledValues" for="edge">
    <desc>Labeled Upper-Case Values. Format: {[('node name (no case modification)', 'integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <key id="Value" for="edge">
    <desc>Value for STN edge. Format: 'integer'</desc>
    <default/>
  </key>
  <key id="LabeledValues" for="edge">
    <desc>Labeled Values. Format: {[('integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <graph edgedefault="directed">
    <node id="S_XOR_1_G2">
      
      <data key="x">421</data>
      <data key="y">156</data>
      <data key="Label">⊡</data>
    </node>
    <node id="E_XOR_1_G2">
      
      <data key="x">469</data>
      <data key="y">154</data>
      <data key="Label">⊡</data>
    </node>
    <edge id="S_XOR_1_G2-E_XOR_1_G2" source="S_XOR_1_G2" target="E_XOR_1_G2">
      
      <data key="Type">normal</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_XOR_1_G2-S_XOR_1_G2" source="E_XOR_1_G2" target="S_XOR_1_G2">
      
      <data key="Type">normal</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_TASK_1_T3">
      
      <data key="x">246</data>
      <data key="y">251</data>
      <data key="Label">¬A</data>
    </node>
    <node id="E_TASK_1_T3">
      
      <data key="x">294</data>
      <data key="y">249</data>
      <data key="Label">¬A</data>
    </node>
    <edge id="S_TASK_1_T3-E_TASK_1_T3" source="S_TASK_1_T3" target="E_TASK_1_T3">
      
      <data key="Type">contingent</data>
      <data key="Value">6</data>
    </edge>
    <edge id="E_TASK_1_T3-S_TASK_1_T3" source="E_TASK_1_T3" target="S_TASK_1_T3">
      
      <data key="Type">contingent</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_TASK_2_T4">
      
      <data key="x">537</data>
      <data key="y">142</data>
      <data key="Label">⊡</data>
    </node>
    <node id="E_TASK_2_T4">
      
      <data key="x">583</data>
      <data key="y">138</data>
      <data key="Label">⊡</data>
    </node>
    <edge id="S_TASK_2_T4-E_TASK_2_T4" source="S_TASK_2_T4" target="E_TASK_2_T4">
      
      <data key="Type">contingent</data>
      <data key="Value">4</data>
    </edge>
    <edge id="E_TASK_2_T4-S_TASK_2_T4" source="E_TASK_2_T4" target="S_TASK_2_T4">
      
      <data key="Type">contingent</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_TASK_3_IE1">
      
      <data key="x">430</data>
      <data key="y">35</data>
      <data key="Label">A</data>
    </node>
    <node id="E_TASK_3_IE1">
      
      <data key="x">474</data>
      <data key="y">29</data>
      <data key="Label">A</data>
    </node>
    <edge id="S_TASK_3_IE1-E_TASK_3_IE1" source="S_TASK_3_IE1" target="E_TASK_3_IE1">
      
      <data key="Type">contingent</data>
      <data key="Value">3</data>
    </edge>
    <edge id="E_TASK_3_IE1-S_TASK_3_IE1" source="E_TASK_3_IE1" target="S_TASK_3_IE1">
      
      <data key="Type">contingent</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_TASK_4_T1">
      
      <data key="x">149</data>
      <data key="y">144</data>
      <data key="Label">⊡</data>
    </node>
    <node id="E_TASK_4_T1">
      
      <data key="x">191</data>
      <data key="y">136</data>
      <data key="Label">⊡</data>
    </node>
    <edge id="S_TASK_4_T1-E_TASK_4_T1" source="S_TASK_4_T1" target="E_TASK_4_T1">
      
      <data key="Type">contingent</data>
      <data key="Value">3</data>
    </edge>
    <edge id="E_TASK_4_T1-S_TASK_4_T1" source="E_TASK_4_T1" target="S_TASK_4_T1">
      
      <data key="Type">contingent</data>
      <data key="Value">-2</data>
    </edge>
    <node id="S_TASK_5_T2">
      
      <data key="x">250</data>
      <data key="y">15</data>
      <data key="Label">A</data>
    </node>
    <node id="E_TASK_5_T2">
      
      <data key="x">290</data>
      <data key="y">5</data>
      <data key="Label">A</data>
    </node>
    <edge id="S_TASK_5_T2-E_TASK_5_T2" source="S_TASK_5_T2" target="E_TASK_5_T2">
      
      <data key="Type">contingent</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_TASK_5_T2-S_TASK_5_T2" source="E_TASK_5_T2" target="S_TASK_5_T2">
      
      <data key="Type">contingent</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_XOR_2_G1">
      
      <data key="x">272</data>
      <data key="y">157</data>
      <data key="Label">⊡</data>
    </node>
    <node id="E_XOR_2_G1">
      
      <data key="x">318</data>
      <data key="y">153</data>
      <data key="Label">⊡</data>
      <data key="Obs">A</data>
    </node>
    <edge id="S_XOR_2_G1-E_XOR_2_G1" source="S_XOR_2_G1" target="E_XOR_2_G1">
      
      <data key="Type">normal</data>
      <data key="Value">4</data>
    </edge>
    <edge id="E_XOR_2_G1-S_XOR_2_G1" source="E_XOR_2_G1" target="S_XOR_2_G1">
      
      <data key="Type">normal</data>
      <data key="Value">-2</data>
    </edge>
    <node id="Z">
      
      <data key="x">112</data>
      <data key="y">162</data>
    </node>
    <node id="Ω">
      
      <data key="x">712</data>
      <data key="y">162</data>
    </node>
    <edge id="S_XOR_1_G2-E_TASK_1_T3" source="S_XOR_1_G2" target="E_TASK_1_T3">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_XOR_1_G2-E_TASK_3_IE1" source="S_XOR_1_G2" target="E_TASK_3_IE1">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="Ω-E_TASK_2_T4" source="Ω" target="E_TASK_2_T4">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_TASK_3_IE1-E_TASK_5_T2" source="S_TASK_3_IE1" target="E_TASK_5_T2">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_TASK_4_T1-Z" source="S_TASK_4_T1" target="Z">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="E_TASK_4_T1-S_XOR_2_G1" source="E_TASK_4_T1" target="S_XOR_2_G1">
      
      <data key="Type">normal</data>
      <data key="Value">2</data>
    </edge>
    <edge id="S_XOR_2_G1-E_TASK_4_T1" source="S_XOR_2_G1" target="E_TASK_4_T1">
      
      <data key="Type">normal</data>
      <data key="Value">-1</data>
    </edge>
    <edge id="E_XOR_2_G1-S_TASK_5_T2" source="E_XOR_2_G1" target="S_TASK_5_T2">
      
      <data key="Type">normal</data>
      <data key="Value">4</data>
    </edge>
    <edge id="S_TASK_5_T2-E_XOR_2_G1" source="S_TASK_5_T2" target="E_XOR_2_G1">
      
      <data key="Type">normal</data>
      <data key="Value">-1</data>
    </edge>
    <edge id="E_XOR_2_G1-S_TASK_1_T3" source="E_XOR_2_G1" target="S_TASK_1_T3">
      
      <data key="Type">normal</data>
      <data key="Value">3</data>
    </edge>
    <edge id="S_TASK_1_T3-E_XOR_2_G1" source="S_TASK_1_T3" target="E_XOR_2_G1">
      
      <data key="Type">normal</data>
      <data key="Value">-1</data>
    </edge>
    <edge id="S_TASK_2_T4-E_XOR_1_G2" source="S_TASK_2_T4" target="E_XOR_1_G2">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="E_TASK_4_T1-S_TASK_2_T4-RelativeConstraint_2v93" source="E_TASK_4_T1" target="S_TASK_2_T4">
      
      <data key="Type">normal </data>
      <data key="LabeledValues">{(15, ⊡) }</data>
    </edge>
    <edge id="S_TASK_2_T4-E_TASK_4_T1-RelativeConstraint_2v93" source="S_TASK_2_T4" target="E_TASK_4_T1">
      
      <data key="Type">normal</data>
      <data key="LabeledValues">{(-2, ⊡) }</data>
    </edge>
    <data key="nContingent">5</data>
    <data key="nObservedProposition">1</data>
    <data key="NetworkType">CSTNU</data>
    <data key="nEdges">28</data>
    <data key="nVertices">16</data>
    <data key="Name">diagram</data>
  </graph>
</graphml>`;

  let xor_bei_xor_tc = `<?xml version="1.0" encoding="UTF-8"?>
  <bpmn2:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:tempcon="https://gitlab.com/univr.di/TimeAwareBPMN/-/blob/main/model/TABPMN20.xsd" id="sample-diagram" targetNamespace="http://bpmn.io/schema/bpmn" xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd">
    <bpmn2:process id="Process_1" isExecutable="false">
      <bpmn2:startEvent id="StartEvent_1">
        <bpmn2:extensionElements>
          <tempcon:relativeConstraint type="custom:connection" id_relativeConstraint="InterTask_68x5" waypoints="[{&#34;x&#34;:32,&#34;y&#34;:271},{&#34;x&#34;:270,&#34;y&#34;:520},{&#34;x&#34;:499,&#34;y&#34;:282}]">
            <tempcon:target>G2</tempcon:target>
            <tempcon:tDuration />
          </tempcon:relativeConstraint>
        </bpmn2:extensionElements>
        <bpmn2:outgoing>Flow_0jpqrq5</bpmn2:outgoing>
      </bpmn2:startEvent>
      <bpmn2:userTask id="A" name="A">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel>A</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_1eyqzbq</bpmn2:incoming>
        <bpmn2:outgoing>Flow_1vrtr88</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:exclusiveGateway id="G3" name="G3">
        <bpmn2:extensionElements>
          <tempcon:tGateway>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel>A</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tGateway>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_1vrtr88</bpmn2:incoming>
        <bpmn2:incoming>Flow_16hk73t</bpmn2:incoming>
        <bpmn2:outgoing>Flow_1890n2z</bpmn2:outgoing>
      </bpmn2:exclusiveGateway>
      <bpmn2:sequenceFlow id="Flow_1vrtr88" sourceRef="A" targetRef="G3" />
      <bpmn2:userTask id="BP" name="BP">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel>Aa</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_117eefi</bpmn2:incoming>
        <bpmn2:outgoing>Flow_16hk73t</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:sequenceFlow id="Flow_117eefi" sourceRef="BoundaryEvent_01" targetRef="BP" />
      <bpmn2:sequenceFlow id="Flow_16hk73t" sourceRef="BP" targetRef="G3" />
      <bpmn2:boundaryEvent id="BoundaryEvent_01" name="B" attachedToRef="A">
        <bpmn2:extensionElements>
          <tempcon:tEvent>
            <tempcon:observedProposition>a</tempcon:observedProposition>
            <tempcon:tDuration>
              <tempcon:minDuration>0</tempcon:minDuration>
              <tempcon:maxDuration>1</tempcon:maxDuration>
              <tempcon:propositionalLabel>A</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tEvent>
        </bpmn2:extensionElements>
        <bpmn2:outgoing>Flow_117eefi</bpmn2:outgoing>
        <bpmn2:messageEventDefinition id="MessageEventDefinition_0b77ae5" />
      </bpmn2:boundaryEvent>
      <bpmn2:userTask id="C" name="C">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel>¬A</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_0dn59i6</bpmn2:incoming>
        <bpmn2:outgoing>Flow_0orygky</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:sequenceFlow id="Flow_0orygky" sourceRef="C" targetRef="G2" />
      <bpmn2:exclusiveGateway id="G2" name="G2">
        <bpmn2:extensionElements>
          <tempcon:tGateway>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tGateway>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_0orygky</bpmn2:incoming>
        <bpmn2:incoming>Flow_1890n2z</bpmn2:incoming>
        <bpmn2:outgoing>Flow_1jhny4o</bpmn2:outgoing>
      </bpmn2:exclusiveGateway>
      <bpmn2:exclusiveGateway id="G1" name="G1">
        <bpmn2:extensionElements>
          <tempcon:tGateway>
            <tempcon:observedProposition>A</tempcon:observedProposition>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tGateway>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_0jpqrq5</bpmn2:incoming>
        <bpmn2:outgoing>Flow_0dn59i6</bpmn2:outgoing>
        <bpmn2:outgoing>Flow_1eyqzbq</bpmn2:outgoing>
      </bpmn2:exclusiveGateway>
      <bpmn2:sequenceFlow id="Flow_0dn59i6" name="False" sourceRef="G1" targetRef="C">
        <bpmn2:extensionElements>
          <tempcon:tSequenceFlow>
            <tempcon:isTrueBranch>false</tempcon:isTrueBranch>
            <tempcon:tDuration />
          </tempcon:tSequenceFlow>
        </bpmn2:extensionElements>
      </bpmn2:sequenceFlow>
      <bpmn2:sequenceFlow id="Flow_0jpqrq5" sourceRef="StartEvent_1" targetRef="G1" />
      <bpmn2:sequenceFlow id="Flow_1eyqzbq" name="True" sourceRef="G1" targetRef="A">
        <bpmn2:extensionElements>
          <tempcon:tSequenceFlow>
            <tempcon:isTrueBranch>true</tempcon:isTrueBranch>
            <tempcon:tDuration />
          </tempcon:tSequenceFlow>
        </bpmn2:extensionElements>
      </bpmn2:sequenceFlow>
      <bpmn2:sequenceFlow id="Flow_1890n2z" sourceRef="G3" targetRef="G2" />
      <bpmn2:endEvent id="Event_0tmwunq">
        <bpmn2:incoming>Flow_1jhny4o</bpmn2:incoming>
      </bpmn2:endEvent>
      <bpmn2:sequenceFlow id="Flow_1jhny4o" sourceRef="G2" targetRef="Event_0tmwunq" />
    </bpmn2:process>
    <bpmndi:BPMNDiagram id="BPMNDiagram_1">
      <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
        <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1">
          <dc:Bounds x="2" y="240" width="36" height="36" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Activity_0oer724_di" bpmnElement="A">
          <dc:Bounds x="190" y="88" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Gateway_10liglp_di" bpmnElement="G3" isMarkerVisible="true">
          <dc:Bounds x="405" y="103" width="50" height="50" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="423" y="73" width="15" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_1vrtr88_di" bpmnElement="Flow_1vrtr88">
          <di:waypoint x="290" y="128" />
          <di:waypoint x="405" y="128" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Activity_1koz9gt_di" bpmnElement="BP">
          <dc:Bounds x="300" y="210" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_117eefi_di" bpmnElement="Flow_117eefi">
          <di:waypoint x="270" y="186" />
          <di:waypoint x="270" y="250" />
          <di:waypoint x="300" y="250" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_16hk73t_di" bpmnElement="Flow_16hk73t">
          <di:waypoint x="400" y="250" />
          <di:waypoint x="430" y="250" />
          <di:waypoint x="430" y="153" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Event_09nczpf_di" bpmnElement="BoundaryEvent_01">
          <dc:Bounds x="252" y="150" width="36" height="36" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="286" y="193" width="8" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Activity_1totojd_di" bpmnElement="C">
          <dc:Bounds x="250" y="350" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_0orygky_di" bpmnElement="Flow_0orygky">
          <di:waypoint x="350" y="390" />
          <di:waypoint x="500" y="390" />
          <di:waypoint x="500" y="283" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Gateway_0hv6ywv_di" bpmnElement="G2" isMarkerVisible="true">
          <dc:Bounds x="475" y="233" width="50" height="50" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="472" y="273" width="15" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Gateway_1r45lw3_di" bpmnElement="G1" isMarkerVisible="true">
          <dc:Bounds x="95" y="233" width="50" height="50" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="132" y="273" width="15" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_0dn59i6_di" bpmnElement="Flow_0dn59i6">
          <di:waypoint x="120" y="283" />
          <di:waypoint x="120" y="390" />
          <di:waypoint x="250" y="390" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="174" y="335" width="27" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_0jpqrq5_di" bpmnElement="Flow_0jpqrq5">
          <di:waypoint x="38" y="258" />
          <di:waypoint x="95" y="258" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_1eyqzbq_di" bpmnElement="Flow_1eyqzbq">
          <di:waypoint x="120" y="233" />
          <di:waypoint x="120" y="128" />
          <di:waypoint x="190" y="128" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="124" y="178" width="23" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_1890n2z_di" bpmnElement="Flow_1890n2z">
          <di:waypoint x="455" y="128" />
          <di:waypoint x="500" y="128" />
          <di:waypoint x="500" y="233" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Event_0tmwunq_di" bpmnElement="Event_0tmwunq">
          <dc:Bounds x="592" y="240" width="36" height="36" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_1jhny4o_di" bpmnElement="Flow_1jhny4o">
          <di:waypoint x="525" y="258" />
          <di:waypoint x="592" y="258" />
        </bpmndi:BPMNEdge>
      </bpmndi:BPMNPlane>
    </bpmndi:BPMNDiagram>
  </bpmn2:definitions>`;
  let xor_bei_xor_tc_translated = `<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns/graphml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns/graphml">
  <key id="nContingent" for="graph">
    <desc>Number of contingents in the graph</desc>
    <default>0</default>
  </key>
  <key id="nObservedProposition" for="graph">
    <desc>Number of observed propositions in the graph</desc>
    <default>0</default>
  </key>
  <key id="NetworkType" for="graph">
    <desc>Network Type</desc>
    <default>CSTNU</default>
  </key>
  <key id="nEdges" for="graph">
    <desc>Number of edges in the graph</desc>
    <default>0</default>
  </key>
  <key id="nVertices" for="graph">
    <desc>Number of vertices in the graph</desc>
    <default>0</default>
  </key>
  <key id="Name" for="graph">
    <desc>Graph Name</desc>
    <default/>
  </key>
  <key id="Obs" for="node">
    <desc>Proposition Observed. Value specification: [a-zA-F]</desc>
    <default/>
  </key>
  <key id="x" for="node">
    <desc>The x coordinate for the visualitation. A positive value.</desc>
    <default>0</default>
  </key>
  <key id="Label" for="node">
    <desc>Label. Format: [¬[a-zA-F]|[a-zA-F]]+|⊡</desc>
    <default>⊡</default>
  </key>
  <key id="y" for="node">
    <desc>The y coordinate for the visualitation. A positive value.</desc>
    <default>0</default>
  </key>
  <key id="Potential" for="node">
    <desc>Labeled Potential Values. Format: {[('node name (no case modification)', 'integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <key id="Type" for="edge">
    <desc>Type: Possible values: normal|contingent|constraint|derived|internal.</desc>
    <default>normal</default>
  </key>
  <key id="LowerCaseLabeledValues" for="edge">
    <desc>Labeled Lower-Case Values. Format: {[('node name (no case modification)', 'integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <key id="UpperCaseLabeledValues" for="edge">
    <desc>Labeled Upper-Case Values. Format: {[('node name (no case modification)', 'integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <key id="Value" for="edge">
    <desc>Value for STN edge. Format: 'integer'</desc>
    <default/>
  </key>
  <key id="LabeledValues" for="edge">
    <desc>Labeled Values. Format: {[('integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <graph edgedefault="directed">
    <node id="S_TASK_1_A">
      
      <data key="x">166</data>
      <data key="y">89</data>
      <data key="Label">A</data>
    </node>
    <node id="E_TASK_1_A">
      
      <data key="x">214</data>
      <data key="y">87</data>
      <data key="Label">A</data>
    </node>
    <edge id="S_TASK_1_A-E_TASK_1_A" source="S_TASK_1_A" target="E_TASK_1_A">
      
      <data key="Type">contingent</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_TASK_1_A-S_TASK_1_A" source="E_TASK_1_A" target="S_TASK_1_A">
      
      <data key="Type">contingent</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_XOR_1_G3">
      
      <data key="x">381</data>
      <data key="y">104</data>
      <data key="Label">A</data>
    </node>
    <node id="E_XOR_1_G3">
      
      <data key="x">429</data>
      <data key="y">102</data>
      <data key="Label">A</data>
    </node>
    <edge id="S_XOR_1_G3-E_XOR_1_G3" source="S_XOR_1_G3" target="E_XOR_1_G3">
      
      <data key="Type">normal</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_XOR_1_G3-S_XOR_1_G3" source="E_XOR_1_G3" target="S_XOR_1_G3">
      
      <data key="Type">normal</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_TASK_2_BP">
      
      <data key="x">277</data>
      <data key="y">212</data>
      <data key="Label">Aa</data>
    </node>
    <node id="E_TASK_2_BP">
      
      <data key="x">323</data>
      <data key="y">208</data>
      <data key="Label">Aa</data>
    </node>
    <edge id="S_TASK_2_BP-E_TASK_2_BP" source="S_TASK_2_BP" target="E_TASK_2_BP">
      
      <data key="Type">contingent</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_TASK_2_BP-S_TASK_2_BP" source="E_TASK_2_BP" target="S_TASK_2_BP">
      
      <data key="Type">contingent</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_TASK_3_C">
      
      <data key="x">228</data>
      <data key="y">353</data>
      <data key="Label">¬A</data>
    </node>
    <node id="E_TASK_3_C">
      
      <data key="x">272</data>
      <data key="y">347</data>
      <data key="Label">¬A</data>
    </node>
    <edge id="S_TASK_3_C-E_TASK_3_C" source="S_TASK_3_C" target="E_TASK_3_C">
      
      <data key="Type">contingent</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_TASK_3_C-S_TASK_3_C" source="E_TASK_3_C" target="S_TASK_3_C">
      
      <data key="Type">contingent</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_XOR_2_G2">
      
      <data key="x">452</data>
      <data key="y">235</data>
      <data key="Label">⊡</data>
    </node>
    <node id="E_XOR_2_G2">
      
      <data key="x">498</data>
      <data key="y">231</data>
      <data key="Label">⊡</data>
    </node>
    <edge id="S_XOR_2_G2-E_XOR_2_G2" source="S_XOR_2_G2" target="E_XOR_2_G2">
      
      <data key="Type">normal</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_XOR_2_G2-S_XOR_2_G2" source="E_XOR_2_G2" target="S_XOR_2_G2">
      
      <data key="Type">normal</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_XOR_3_G1">
      
      <data key="x">73</data>
      <data key="y">236</data>
      <data key="Label">⊡</data>
    </node>
    <node id="E_XOR_3_G1">
      
      <data key="x">117</data>
      <data key="y">230</data>
      <data key="Label">⊡</data>
      <data key="Obs">A</data>
    </node>
    <edge id="S_XOR_3_G1-E_XOR_3_G1" source="S_XOR_3_G1" target="E_XOR_3_G1">
      
      <data key="Type">normal</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_XOR_3_G1-S_XOR_3_G1" source="E_XOR_3_G1" target="S_XOR_3_G1">
      
      <data key="Type">normal</data>
      <data key="Value">-1</data>
    </edge>
    <node id="Z">
      
      <data key="x">2</data>
      <data key="y">240</data>
    </node>
    <node id="S_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="x">228</data>
      <data key="y">151</data>
      <data key="Label">A</data>
    </node>
    <node id="E_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="x">276</data>
      <data key="y">149</data>
      <data key="Label">A</data>
      <data key="Obs">a</data>
    </node>
    <edge id="S_BOUNDARY_1_BoundaryEvent_01-E_BOUNDARY_1_BoundaryEvent_01" source="S_BOUNDARY_1_BoundaryEvent_01" target="E_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="Type">normal</data>
      <data key="Value">1</data>
    </edge>
    <edge id="E_BOUNDARY_1_BoundaryEvent_01-S_BOUNDARY_1_BoundaryEvent_01" source="E_BOUNDARY_1_BoundaryEvent_01" target="S_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <node id="Ω">
      
      <data key="x">592</data>
      <data key="y">240</data>
    </node>
    <edge id="S_XOR_1_G3-E_TASK_1_A" source="S_XOR_1_G3" target="E_TASK_1_A">
      <data key="Type">normal</data>
      <data key="LabeledValues">{(0, A¬a) }</data>
    </edge>
    <edge id="S_TASK_2_BP-E_BOUNDARY_1_BoundaryEvent_01" source="S_TASK_2_BP" target="E_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_XOR_1_G3-E_TASK_2_BP" source="S_XOR_1_G3" target="E_TASK_2_BP">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_XOR_2_G2-E_TASK_3_C" source="S_XOR_2_G2" target="E_TASK_3_C">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_TASK_3_C-E_XOR_3_G1" source="S_TASK_3_C" target="E_XOR_3_G1">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_XOR_3_G1-Z" source="S_XOR_3_G1" target="Z">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_TASK_1_A-E_XOR_3_G1" source="S_TASK_1_A" target="E_XOR_3_G1">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_XOR_2_G2-E_XOR_1_G3" source="S_XOR_2_G2" target="E_XOR_1_G3">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="Ω-E_XOR_2_G2" source="Ω" target="E_XOR_2_G2">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_TASK_1_A-S_BOUNDARY_1_BoundaryEvent_01" source="S_TASK_1_A" target="S_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_BOUNDARY_1_BoundaryEvent_01-S_TASK_1_A" source="S_BOUNDARY_1_BoundaryEvent_01" target="S_TASK_1_A">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <data key="nContingent">3</data>
    <data key="nObservedProposition">2</data>
    <data key="NetworkType">CSTNU</data>
    <data key="nEdges">25</data>
    <data key="nVertices">16</data>
    <data key="Name">diagram</data>
  </graph>
</graphml>`;
  let xor_beni_xor_tc = `<?xml version="1.0" encoding="UTF-8"?>
  <bpmn2:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:tempcon="https://gitlab.com/univr.di/TimeAwareBPMN/-/blob/main/model/TABPMN20.xsd" id="sample-diagram" targetNamespace="http://bpmn.io/schema/bpmn" xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd">
    <bpmn2:process id="Process_1" isExecutable="false">
      <bpmn2:startEvent id="StartEvent_1">
        <bpmn2:extensionElements>
          <tempcon:relativeConstraint type="custom:connection" id_relativeConstraint="InterTask_4gfv" waypoints="[{&#34;x&#34;:-90,&#34;y&#34;:240},{&#34;x&#34;:-90,&#34;y&#34;:99},{&#34;x&#34;:470,&#34;y&#34;:101},{&#34;x&#34;:470,&#34;y&#34;:233}]">
            <tempcon:target>G2</tempcon:target>
            <tempcon:tDuration />
          </tempcon:relativeConstraint>
        </bpmn2:extensionElements>
        <bpmn2:outgoing>Flow_0jpqrq5</bpmn2:outgoing>
      </bpmn2:startEvent>
      <bpmn2:userTask id="A" name="A">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel>A</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_020mc4x</bpmn2:incoming>
        <bpmn2:outgoing>Flow_0rl69wh</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:userTask id="BP" name="BP">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel>Aa</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_117eefi</bpmn2:incoming>
        <bpmn2:outgoing>Flow_1s2ui2r</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:sequenceFlow id="Flow_117eefi" sourceRef="BoundaryEvent_01" targetRef="BP" />
      <bpmn2:endEvent id="Event_18q35wv">
        <bpmn2:incoming>Flow_1532vbc</bpmn2:incoming>
      </bpmn2:endEvent>
      <bpmn2:exclusiveGateway id="G1" name="G1">
        <bpmn2:extensionElements>
          <tempcon:tGateway>
            <tempcon:observedProposition>A</tempcon:observedProposition>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tGateway>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_0jpqrq5</bpmn2:incoming>
        <bpmn2:outgoing>Flow_0dn59i6</bpmn2:outgoing>
        <bpmn2:outgoing>Flow_020mc4x</bpmn2:outgoing>
      </bpmn2:exclusiveGateway>
      <bpmn2:sequenceFlow id="Flow_0jpqrq5" sourceRef="StartEvent_1" targetRef="G1" />
      <bpmn2:exclusiveGateway id="G2" name="G2">
        <bpmn2:extensionElements>
          <tempcon:tGateway>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel></tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tGateway>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_0orygky</bpmn2:incoming>
        <bpmn2:incoming>Flow_0bn8l1y</bpmn2:incoming>
        <bpmn2:outgoing>Flow_1532vbc</bpmn2:outgoing>
      </bpmn2:exclusiveGateway>
      <bpmn2:userTask id="C" name="C">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel>¬A</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_0dn59i6</bpmn2:incoming>
        <bpmn2:outgoing>Flow_0orygky</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:sequenceFlow id="Flow_0dn59i6" name="False" sourceRef="G1" targetRef="C">
        <bpmn2:extensionElements>
          <tempcon:tSequenceFlow>
            <tempcon:isTrueBranch>false</tempcon:isTrueBranch>
            <tempcon:tDuration />
          </tempcon:tSequenceFlow>
        </bpmn2:extensionElements>
      </bpmn2:sequenceFlow>
      <bpmn2:sequenceFlow id="Flow_0orygky" sourceRef="C" targetRef="G2" />
      <bpmn2:endEvent id="Event_150yops">
        <bpmn2:incoming>Flow_1s2ui2r</bpmn2:incoming>
      </bpmn2:endEvent>
      <bpmn2:sequenceFlow id="Flow_1s2ui2r" sourceRef="BP" targetRef="Event_150yops" />
      <bpmn2:boundaryEvent id="BoundaryEvent_01" name="B" cancelActivity="false" attachedToRef="A">
        <bpmn2:extensionElements>
          <tempcon:tEvent>
            <tempcon:observedProposition>a</tempcon:observedProposition>
            <tempcon:tDuration>
              <tempcon:minDuration>0</tempcon:minDuration>
              <tempcon:maxDuration>1</tempcon:maxDuration>
              <tempcon:propositionalLabel>A</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tEvent>
        </bpmn2:extensionElements>
        <bpmn2:outgoing>Flow_117eefi</bpmn2:outgoing>
        <bpmn2:messageEventDefinition id="MessageEventDefinition_1jxiyye" />
      </bpmn2:boundaryEvent>
      <bpmn2:userTask id="D" name="D">
        <bpmn2:extensionElements>
          <tempcon:tTask>
            <tempcon:tDuration>
              <tempcon:minDuration>1</tempcon:minDuration>
              <tempcon:maxDuration>2</tempcon:maxDuration>
              <tempcon:propositionalLabel>A</tempcon:propositionalLabel>
            </tempcon:tDuration>
          </tempcon:tTask>
        </bpmn2:extensionElements>
        <bpmn2:incoming>Flow_0rl69wh</bpmn2:incoming>
        <bpmn2:outgoing>Flow_0bn8l1y</bpmn2:outgoing>
      </bpmn2:userTask>
      <bpmn2:sequenceFlow id="Flow_0rl69wh" sourceRef="A" targetRef="D" />
      <bpmn2:sequenceFlow id="Flow_020mc4x" name="True" sourceRef="G1" targetRef="A">
        <bpmn2:extensionElements>
          <tempcon:tSequenceFlow>
            <tempcon:isTrueBranch>true</tempcon:isTrueBranch>
            <tempcon:tDuration />
          </tempcon:tSequenceFlow>
        </bpmn2:extensionElements>
      </bpmn2:sequenceFlow>
      <bpmn2:sequenceFlow id="Flow_0bn8l1y" sourceRef="D" targetRef="G2" />
      <bpmn2:sequenceFlow id="Flow_1532vbc" sourceRef="G2" targetRef="Event_18q35wv" />
    </bpmn2:process>
    <bpmndi:BPMNDiagram id="BPMNDiagram_1">
      <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
        <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="StartEvent_1">
          <dc:Bounds x="-108" y="240" width="36" height="36" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Activity_0oer724_di" bpmnElement="A">
          <dc:Bounds x="90" y="218" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Activity_1koz9gt_di" bpmnElement="BP">
          <dc:Bounds x="200" y="340" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_117eefi_di" bpmnElement="Flow_117eefi">
          <di:waypoint x="170" y="316" />
          <di:waypoint x="170" y="380" />
          <di:waypoint x="200" y="380" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Event_18q35wv_di" bpmnElement="Event_18q35wv">
          <dc:Bounds x="582" y="240" width="36" height="36" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Gateway_1r45lw3_di" bpmnElement="G1" isMarkerVisible="true">
          <dc:Bounds x="-15" y="233" width="50" height="50" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="22" y="273" width="15" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_0jpqrq5_di" bpmnElement="Flow_0jpqrq5">
          <di:waypoint x="-72" y="258" />
          <di:waypoint x="-15" y="258" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Gateway_0hv6ywv_di" bpmnElement="G2" isMarkerVisible="true">
          <dc:Bounds x="445" y="233" width="50" height="50" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="442" y="273" width="15" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Activity_1totojd_di" bpmnElement="C">
          <dc:Bounds x="200" y="480" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_0dn59i6_di" bpmnElement="Flow_0dn59i6">
          <di:waypoint x="10" y="283" />
          <di:waypoint x="10" y="520" />
          <di:waypoint x="200" y="520" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="12" y="407" width="27" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_0orygky_di" bpmnElement="Flow_0orygky">
          <di:waypoint x="300" y="520" />
          <di:waypoint x="470" y="520" />
          <di:waypoint x="470" y="283" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Event_150yops_di" bpmnElement="Event_150yops">
          <dc:Bounds x="332" y="352" width="36" height="36" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_1s2ui2r_di" bpmnElement="Flow_1s2ui2r">
          <di:waypoint x="300" y="370" />
          <di:waypoint x="332" y="370" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNShape id="Event_1dm9cce_di" bpmnElement="BoundaryEvent_01">
          <dc:Bounds x="152" y="280" width="36" height="36" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="186" y="323" width="8" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNShape>
        <bpmndi:BPMNShape id="Activity_0solf7e_di" bpmnElement="D">
          <dc:Bounds x="280" y="218" width="100" height="80" />
        </bpmndi:BPMNShape>
        <bpmndi:BPMNEdge id="Flow_0rl69wh_di" bpmnElement="Flow_0rl69wh">
          <di:waypoint x="190" y="258" />
          <di:waypoint x="280" y="258" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_020mc4x_di" bpmnElement="Flow_020mc4x">
          <di:waypoint x="35" y="258" />
          <di:waypoint x="90" y="258" />
          <bpmndi:BPMNLabel>
            <dc:Bounds x="51" y="240" width="23" height="14" />
          </bpmndi:BPMNLabel>
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_0bn8l1y_di" bpmnElement="Flow_0bn8l1y">
          <di:waypoint x="380" y="258" />
          <di:waypoint x="445" y="258" />
        </bpmndi:BPMNEdge>
        <bpmndi:BPMNEdge id="Flow_1532vbc_di" bpmnElement="Flow_1532vbc">
          <di:waypoint x="495" y="258" />
          <di:waypoint x="582" y="258" />
        </bpmndi:BPMNEdge>
      </bpmndi:BPMNPlane>
    </bpmndi:BPMNDiagram>
  </bpmn2:definitions>`;
  let xor_beni_xor_tc_translated = `<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns/graphml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns/graphml">
  <key id="nContingent" for="graph">
    <desc>Number of contingents in the graph</desc>
    <default>0</default>
  </key>
  <key id="nObservedProposition" for="graph">
    <desc>Number of observed propositions in the graph</desc>
    <default>0</default>
  </key>
  <key id="NetworkType" for="graph">
    <desc>Network Type</desc>
    <default>CSTNU</default>
  </key>
  <key id="nEdges" for="graph">
    <desc>Number of edges in the graph</desc>
    <default>0</default>
  </key>
  <key id="nVertices" for="graph">
    <desc>Number of vertices in the graph</desc>
    <default>0</default>
  </key>
  <key id="Name" for="graph">
    <desc>Graph Name</desc>
    <default/>
  </key>
  <key id="Obs" for="node">
    <desc>Proposition Observed. Value specification: [a-zA-F]</desc>
    <default/>
  </key>
  <key id="x" for="node">
    <desc>The x coordinate for the visualitation. A positive value.</desc>
    <default>0</default>
  </key>
  <key id="Label" for="node">
    <desc>Label. Format: [¬[a-zA-F]|[a-zA-F]]+|⊡</desc>
    <default>⊡</default>
  </key>
  <key id="y" for="node">
    <desc>The y coordinate for the visualitation. A positive value.</desc>
    <default>0</default>
  </key>
  <key id="Potential" for="node">
    <desc>Labeled Potential Values. Format: {[('node name (no case modification)', 'integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <key id="Type" for="edge">
    <desc>Type: Possible values: normal|contingent|constraint|derived|internal.</desc>
    <default>normal</default>
  </key>
  <key id="LowerCaseLabeledValues" for="edge">
    <desc>Labeled Lower-Case Values. Format: {[('node name (no case modification)', 'integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <key id="UpperCaseLabeledValues" for="edge">
    <desc>Labeled Upper-Case Values. Format: {[('node name (no case modification)', 'integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <key id="Value" for="edge">
    <desc>Value for STN edge. Format: 'integer'</desc>
    <default/>
  </key>
  <key id="LabeledValues" for="edge">
    <desc>Labeled Values. Format: {[('integer', 'label') ]+}|{}</desc>
    <default/>
  </key>
  <graph edgedefault="directed">
    <node id="S_TASK_1_A">
      
      <data key="x">66</data>
      <data key="y">219</data>
      <data key="Label">A</data>
    </node>
    <node id="E_TASK_1_A_join">
      
      <data key="x">230</data>
      <data key="y">218</data>
      <data key="Label">A</data>
    </node>
    <node id="E_TASK_1_A">
      
      <data key="x">114</data>
      <data key="y">217</data>
      <data key="Label">A</data>
    </node>
    <edge id="S_TASK_1_A-E_TASK_1_A" source="S_TASK_1_A" target="E_TASK_1_A">
      
      <data key="Type">contingent</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_TASK_1_A-S_TASK_1_A" source="E_TASK_1_A" target="S_TASK_1_A">
      
      <data key="Type">contingent</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_TASK_2_BP">
      
      <data key="x">177</data>
      <data key="y">342</data>
      <data key="Label">Aa</data>
    </node>
    <node id="E_TASK_2_BP">
      
      <data key="x">223</data>
      <data key="y">338</data>
      <data key="Label">Aa</data>
    </node>
    <edge id="S_TASK_2_BP-E_TASK_2_BP" source="S_TASK_2_BP" target="E_TASK_2_BP">
      
      <data key="Type">contingent</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_TASK_2_BP-S_TASK_2_BP" source="E_TASK_2_BP" target="S_TASK_2_BP">
      
      <data key="Type">contingent</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_XOR_1_G1">
      
      <data key="x">-39</data>
      <data key="y">234</data>
      <data key="Label">⊡</data>
    </node>
    <node id="E_XOR_1_G1">
      
      <data key="x">9</data>
      <data key="y">232</data>
      <data key="Label">⊡</data>
      <data key="Obs">A</data>
    </node>
    <edge id="S_XOR_1_G1-E_XOR_1_G1" source="S_XOR_1_G1" target="E_XOR_1_G1">
      
      <data key="Type">normal</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_XOR_1_G1-S_XOR_1_G1" source="E_XOR_1_G1" target="S_XOR_1_G1">
      
      <data key="Type">normal</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_XOR_2_G2">
      
      <data key="x">422</data>
      <data key="y">235</data>
      <data key="Label">⊡</data>
    </node>
    <node id="E_XOR_2_G2">
      
      <data key="x">468</data>
      <data key="y">231</data>
      <data key="Label">⊡</data>
    </node>
    <edge id="S_XOR_2_G2-E_XOR_2_G2" source="S_XOR_2_G2" target="E_XOR_2_G2">
      
      <data key="Type">normal</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_XOR_2_G2-S_XOR_2_G2" source="E_XOR_2_G2" target="S_XOR_2_G2">
      
      <data key="Type">normal</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_TASK_3_C">
      
      <data key="x">178</data>
      <data key="y">483</data>
      <data key="Label">¬A</data>
    </node>
    <node id="E_TASK_3_C">
      
      <data key="x">222</data>
      <data key="y">477</data>
      <data key="Label">¬A</data>
    </node>
    <edge id="S_TASK_3_C-E_TASK_3_C" source="S_TASK_3_C" target="E_TASK_3_C">
      
      <data key="Type">contingent</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_TASK_3_C-S_TASK_3_C" source="E_TASK_3_C" target="S_TASK_3_C">
      
      <data key="Type">contingent</data>
      <data key="Value">-1</data>
    </edge>
    <node id="S_TASK_4_D">
      
      <data key="x">259</data>
      <data key="y">222</data>
      <data key="Label">A</data>
    </node>
    <node id="E_TASK_4_D">
      
      <data key="x">301</data>
      <data key="y">214</data>
      <data key="Label">A</data>
    </node>
    <edge id="S_TASK_4_D-E_TASK_4_D" source="S_TASK_4_D" target="E_TASK_4_D">
      
      <data key="Type">contingent</data>
      <data key="Value">2</data>
    </edge>
    <edge id="E_TASK_4_D-S_TASK_4_D" source="E_TASK_4_D" target="S_TASK_4_D">
      
      <data key="Type">contingent</data>
      <data key="Value">-1</data>
    </edge>
    <node id="Z">
      
      <data key="x">-108</data>
      <data key="y">240</data>
    </node>
    <node id="Ω">
      
      <data key="x">582</data>
      <data key="y">240</data>
    </node>
    <node id="Ω1">
      
      <data key="x">333</data>
      <data key="y">353</data>
    </node>
    <node id="S_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="x">128</data>
      <data key="y">281</data>
      <data key="Label">A</data>
    </node>
    <node id="E_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="x">176</data>
      <data key="y">279</data>
      <data key="Label">A</data>
      <data key="Obs">a</data>
    </node>
    <edge id="S_BOUNDARY_1_BoundaryEvent_01-E_BOUNDARY_1_BoundaryEvent_01" source="S_BOUNDARY_1_BoundaryEvent_01" target="E_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="Type">normal</data>
      <data key="Value">1</data>
    </edge>
    <edge id="E_BOUNDARY_1_BoundaryEvent_01-S_BOUNDARY_1_BoundaryEvent_01" source="E_BOUNDARY_1_BoundaryEvent_01" target="S_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_TASK_2_BP-E_BOUNDARY_1_BoundaryEvent_01" source="S_TASK_2_BP" target="E_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_XOR_1_G1-Z" source="S_XOR_1_G1" target="Z">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_TASK_3_C-E_XOR_1_G1" source="S_TASK_3_C" target="E_XOR_1_G1">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_XOR_2_G2-E_TASK_3_C" source="S_XOR_2_G2" target="E_TASK_3_C">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="Ω1-E_TASK_2_BP" source="Ω1" target="E_TASK_2_BP">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_TASK_4_D-E_TASK_1_A" source="S_TASK_4_D" target="E_TASK_1_A_join">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_TASK_1_A-E_XOR_1_G1" source="S_TASK_1_A" target="E_XOR_1_G1">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_XOR_2_G2-E_TASK_4_D" source="S_XOR_2_G2" target="E_TASK_4_D">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="Ω-E_XOR_2_G2" source="Ω" target="E_XOR_2_G2">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_TASK_1_A-S_BOUNDARY_1_BoundaryEvent_01" source="S_TASK_1_A" target="S_BOUNDARY_1_BoundaryEvent_01">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="S_BOUNDARY_1_BoundaryEvent_01-S_TASK_1_A" source="S_BOUNDARY_1_BoundaryEvent_01" target="S_TASK_1_A">
      
      <data key="Type">normal</data>
      <data key="Value">0</data>
    </edge>
    <edge id="E_TASK_1_A-E_TASK_1_A_join" source="E_TASK_1_A" target="E_TASK_1_A_join">
      
      <data key="Type">normal</data>
      <data key="LabeledValues">{(0, A¬a) }</data>
    </edge>
    <edge id="E_TASK_1_A_join-E_TASK_1_A" source="E_TASK_1_A_join" target="E_TASK_1_A">
      
      <data key="Type">normal</data>
      <data key="LabeledValues">{(0, A¬a) (0, Aa) }</data>
    </edge>
    <edge id="E_TASK_1_A_join-E_TASK_2_BP" source="E_TASK_1_A_join" target="E_TASK_2_BP">
      
      <data key="Type">normal</data>
      <data key="LabeledValues">{(0, Aa) }</data>
    </edge>
    <data key="nContingent">4</data>
    <data key="nObservedProposition">2</data>
    <data key="NetworkType">CSTNU</data>
    <data key="nEdges">28</data>
    <data key="nVertices">18</data>
    <data key="Name">diagram</data>
  </graph>
</graphml>`;

  const JSDOM = require("jsdom");
  // import JSDOM from "jsdom";
  // Create empty DOM, the imput param here is for HTML not XML, and we don want to parse HTML
  const dom = new JSDOM.JSDOM("");
  // Get DOMParser, same API as in browser
  const DOMParser = dom.window.DOMParser;
  const parser = new DOMParser;
  // Create document by parsing XML

  if (idDiagram == "example01") {
    const xmlDoc = parser.parseFromString(example01, "text/xml");
    return { diagram: example01, xmlDoc, diagram_translated: example01_translated };
  }
  if (idDiagram == "xor_bei_xor_tc") {
    const xmlDoc = parser.parseFromString(xor_bei_xor_tc, "text/xml");
    return { diagram: xor_bei_xor_tc, xmlDoc, diagram_translated: xor_bei_xor_tc_translated };
  }
  if (idDiagram == "xor_beni_xor_tc") {
    const xmlDoc = parser.parseFromString(xor_beni_xor_tc, "text/xml");
    return { diagram: xor_beni_xor_tc, xmlDoc, diagram_translated: xor_beni_xor_tc_translated };
  }

}