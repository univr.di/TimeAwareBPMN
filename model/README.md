# XML Schema Definition 

This folder contains the XML scheme definition of the `tempcon` elements.
`tempcon` elements represent *temporal constraints*.

A `tempcon` element can be associated with a BPMN element adding it as child of the `extensionElements` of the BPMN element.
