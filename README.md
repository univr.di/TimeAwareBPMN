# TimeAwareBPMN-js
It is a tool for editing and verifying time-aware BPMN models, BPMN models 
having temporal constraints.

It is a web-application and consists of a graphical editor 
([bpmn-js](https://bpmn.io/toolkit/bpmn-js/)) where it is possible to create 
or edit time-aware BPMN models and to select and run a plug-in for *verifying* 
temporal constraints.

Plug-in architecture allows the execution of different programs for verifying 
different temporal properties.

As a proof-of-concept, the application contains the *CSTNU plug-in*, that allows
the verification if the model is *dynamically controllable*, i.e., it is 
possible to execute it whatever the duration of some activities, 
called *contingent* activities.
Each contingent-activity duration is limited to stay in a temporal range, 
but the exact duration is decided by the external "agent" executing it at 
runtime.<br>
CSTNU plug-in verifies the dynamic controllability property using the external 
Java library [CSTNU-Tool](https://profs.scienze.univr.it/~posenato/software/cstnu/).


## Installation
To run the application, it is necessary to install [Node.js](https://nodejs.org/)
*version 16*, and some Node.js dependencies.

1. It is necessary to have the 16th version of Node.js.

	**Installation in Mac OS X**
	
	If `$ node -v` returns a major number > 16, install the 16th version.<br>
	For example, if you are using `brew`, 
	
	```bash
	brew install node@16
	brew link --overwrite node@16
	```
	**Installation in Ubuntu**
	
	If `$ node -v` returns a major number > 16, install the 16th version.<br>
	For example, 
	
	```bash
	curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -
	sudo apt install nodejs
	```
		
2. It is necessary to install the **Java** module (for running the CSTNU plug-in). 

    **Installation in Mac OS X**
    
    > 1. Install JDK 17.<br>
    Using brew, `$ brew install openjdk@17`
    > 2. Set the `JAVA_HOME` environment variable to JDK home.<br>
    ``$ export JAVA_HOME=`/usr/libexec/java_home -v 17` ``<br>
    Hints: In PATH environment variable don't add any path to any Java bin directory.<br>
    Binaries are automatically found considering the JAVA_HOME variable value.
    > 3. Install compiling tools: `npm install -g node-gyp`
    > 4. Install java: `npm install java`<br>
    If java module compilation stops for the error `fatal error: 'jni_md.h' file not found`,
    go to `include` directory of Java distribution and execute `cp darwin/jni_md.h .`
    > 5. Ignore any warning.

    **Installation in Ubuntu**
    > 1. Install JDK 11 
    > 2. Set JAVA_HOME
    > 3. Install compiling tools: `sudo apt install make g++`
    > 4. Install java: `npm install java`<br>

3. Then, install all other dependencies

```bash
npm install
```

Finally, run the project

```bash
npm start
```

### Troubleshooting Installation Issues
-  Error: /usr/bin/ld: cannot find -ljvm <br>
    Check JAVA_HOME is configured correctly.


## Usage
It is possible to create a new model or load one by dragging the file in the 
editor or using the option Load BPMN model. 

This is a screenshot of the interface of the TimeAwareBPMN-js showing 
the graphical editor and the tool with buttons for the possible actions. 

![Screenshot of the interface](./examples/screenshots/screenshotInterface_diagramExample.png)

There are example models in the [models folder](./exmples/models/).

## Demos
[Demos folder](./examples/demos/) contains some videos showing how to use the application.

## Adding a plug-in
The application can be extended, adding other plug-ins for verifying other 
temporal properties (or other properties).
There is an application API to ease the development of plug-in as JavaScript module.
Once a plug-in is developed, it can be used inside the application, just putting 
the JavaScript module in a specific directory.

A plug-in is divided into two parts:
* Client-side plug-in. It has to show the possible actions as buttons in a 
toolbar, prepare the data for the checking, send the data to the corresponding 
server-side plug-in, and receive and show the results.
More details are given at page [client-side plug-in](./TA_BPMN_editor/app/temporal-modeler/temporal-plugins-client/README.md).

* Server-side plug-in. It has to accept the verification requests, realize the 
verification executing some program/library, and return the results to the 
corresponding client-side plug-in.
More details are given at page [server-side plug-in](./temporal-plugins-server/README.md).


## Contributing
Pull requests of new plug-ins and features in the editor are welcome. For major changes in the editor, please open an issue first to discuss what you would like to change.

## License
MIT
